import logging

from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMultiAlternatives
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.text import format_lazy as f
from django.utils.translation import gettext_lazy as _
from rest_framework.request import Request

from access.models import User
from access.tokens import account_activation_token

logger = logging.getLogger('app')


def send_activation_mail(request: Request, user: User):
    subject = _('User Activation')
    content = _(
        "Hello,\n\nThank you for using Remember Me. "
        "To activate your registration, please click on the link below or use it in your browser:\n{link}\n"
        "If you haven't registered, then just ignore this email.\n\nGod bless!")
    url = _server_url(request, user, 'activate')
    link_text = _('Activate User Access')
    _send_access_mail(user, subject, content, url, link_text)


def send_reset_password_mail(user: User):
    subject = _('Link to Reset Your Password')
    content = _("Hello,\n\nThank you for using Remember Me. "
                "To reset your password, please click on the link below or use it in your browser:\n{link}\n"
                "If you haven't requested to reset your password, then just ignore this email.\n\nGod bless!")
    url = _client_url(user, 'reset-password')
    link_text = _('Reset Password')
    _send_access_mail(user, subject, content, url, link_text)


def send_inactivity_notice(user: User, days: int):
    subject = _('User Inactivity')
    content = _("Hello,\n\nYour last activity on Remember Me remem.me was %(days)d days ago.\n"
                "If you don't access remem.me for another 120 days, your account will be suspended. "
                "You can connect to your account by logging in to the Remember Me app or on {link}.\n\n"
                "God bless!") % {'days': days}
    url = "https://web.remem.me"
    link_text = 'web.remem.me'
    _send_access_mail(user, subject, content, url, link_text)


def _send_access_mail(user: User, subject: str, content: str, url: str, link_text: str):
    link = f('{url}<br><a href="{url}">{text}</a>', url=url, text=link_text.upper())
    plain = f(content, link=url)
    html = f('<p>{content}<br><a href="https://www.remem.me">remem.me</a></p>',
             content=f(content, link=link)).replace('\n', '<br>')
    from_email = "Remember Me Support <support@remem.me>"
    msg = EmailMultiAlternatives(subject, plain, from_email, [user.email, ])
    msg.attach_alternative(html, "text/html")
    msg.send()
    logger.info(f'Email sent to {user.email}: {plain}')


def _server_url(request: Request, user: User, callback: str):
    domain = get_current_site(request).domain
    uidb64 = urlsafe_base64_encode(force_bytes(user.pk))
    token = account_activation_token.make_token(user)
    return f'https://{domain}/v1/access/{callback}/{uidb64}/{token}'


def _client_url(user: User, callback: str):
    uidb64 = urlsafe_base64_encode(force_bytes(user.pk))
    token = account_activation_token.make_token(user)
    return f'https://web.remem.me/{callback}?uidb64={uidb64}&token={token}'
