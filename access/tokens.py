import logging

from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.crypto import salted_hmac
from django.utils.http import int_to_base36

from access.models import User

logger = logging.getLogger('app')

class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    """
    Custom token generator for account activation.
    """

    def _make_hash_value(self, user: User, timestamp: int) -> str:
        """
        Create a hash value combining user data and timestamp.
        """
        return f'{user.pk}{user.is_active}{timestamp}{user.email}'

    def _make_token_with_timestamp(self, user: User, timestamp: int, secret: str) -> str:
        """
        Generate a token using user data, timestamp, and a secret key.
        """
        # Convert timestamp to base 36 (6 digits until ~2069)
        ts_b36 = int_to_base36(timestamp)

        # Generate a salted HMAC value
        hash_string = salted_hmac(
            self.key_salt,
            self._make_hash_value(user, timestamp),
            secret=secret,
            algorithm=self.algorithm,
        ).hexdigest()[::6]  # Use every 6th character to shorten the URL

        # Combine timestamp and hash to form the token
        return "%s-%s" % (ts_b36, hash_string)

# Instantiate the token generator
account_activation_token = AccountActivationTokenGenerator()
# https://web.remem.me/reset-password?uidb64=OTA3NDI&token=cga5sn-783cb5f5bcb
# https://rpl.remem.me/v1/access/activate/OTA3NDI/cga5fc-ddb720ed0c00a337