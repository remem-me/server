from cuser.admin import UserAdmin
from django.contrib import admin

from access.models import User


@admin.register(User)
class ReplicationUserAdmin(UserAdmin):
    list_display = (
    'email', 'is_staff', 'is_active', 'ip_address', 'language', 'last_access', 'date_joined', 'last_notice', 'last_login',)
    list_filter = (
    "is_staff", "is_superuser", "is_active", "groups", 'language', 'last_access', 'date_joined', 'last_notice', 'last_login',)
