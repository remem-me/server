from rest_framework.exceptions import NotAuthenticated, AuthenticationFailed
from rest_framework.status import HTTP_401_UNAUTHORIZED
from rest_framework.views import exception_handler


def auth_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if isinstance(exc, (AuthenticationFailed, NotAuthenticated)):
        response.status_code = HTTP_401_UNAUTHORIZED

    return response
