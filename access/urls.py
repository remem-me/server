from django.urls import path, re_path
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

from .views import registration, activation, forgot_password, reset_password, \
    change_email, delete_user

app_name = 'access'
urlpatterns = [
    path('register/', registration, name='registration'),
    path('forgot-password/', forgot_password, name='forgot_password'),
    path('reset-password/', reset_password, name='reset_password'),
    path('change-email/', change_email, name='change_email'),
    path('delete-user/', delete_user, name='delete_user'),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    re_path(  # todo: remove [_-] from token regex
        '^activate/(?P<uidb64>[0-9A-Za-z_-]+)/(?P<token>[0-9A-Za-z_-]{1,20}-[0-9A-Za-z_-]{1,40})/$',
        activation,
        name='activation'),
]
