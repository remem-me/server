import ipaddress
from datetime import date, timedelta
from typing import Optional

from django.contrib.auth import get_user_model
from django.shortcuts import redirect
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.utils.text import format_lazy as f
from django.utils.translation import gettext_lazy as _
from rest_framework import response, decorators, permissions, status

from main.models import Account
from main.util import IdGenerator
from .mails import send_activation_mail, send_reset_password_mail
from .serializers import UserCreateSerializer, PasswordForgotSerializer, \
    PasswordResetSerializer, EmailChangeSerializer, UserDeleteSerializer
from .tokens import account_activation_token

User = get_user_model()


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.AllowAny])
def registration(request):
    ip = _valid_ip_address(request)
    if ip is None:
        return response.Response({'messages': [_('No valid ip address')]}, status.HTTP_400_BAD_REQUEST)
    if _too_many_registrations(ip):
        return response.Response({'messages': [_('Too many open registrations')]}, status.HTTP_400_BAD_REQUEST)
    serializer = UserCreateSerializer(data={**request.data, 'ip_address': ip})
    if not serializer.is_valid():
        return response.Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
    user = serializer.save()
    send_activation_mail(request, user)
    message = f(_('Activation email sent to {email}. Please check your inbox.'), email=user.email)
    return response.Response({'messages': [message]}, status.HTTP_201_CREATED)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.AllowAny])
def forgot_password(request):
    serializer = PasswordForgotSerializer(data={**request.data})
    if serializer.is_valid():
        send_reset_password_mail(serializer.validated_data)
        message = f(_('Email for resetting the password sent to {email}. Please check your inbox.'),
                    email=serializer.validated_data.email)
        return response.Response({'messages': [message]}, status.HTTP_200_OK)
    else:
        messages = serializer.errors
        return response.Response(messages, status.HTTP_404_NOT_FOUND)


def _valid_ip_address(request) -> Optional[str]:
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        address = x_forwarded_for.split(',')[0]
    else:
        address = request.META.get('REMOTE_ADDR')
    try:
        return str(ipaddress.ip_address(address))
    except ValueError:
        return None


def _too_many_registrations(ip):
    return len(User.objects.filter(is_active=False, ip_address=ip, date_joined__gte=date.today() - timedelta(7))) > 20


@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.AllowAny])
def activation(request, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None:
        if user.is_active:
            return redirect('https://web.remem.me/register-success')
        elif account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            _create_default_account(user)
            return redirect('https://web.remem.me/register-success')
            # return response.Response({'messages': [_('User activated')]}, status.HTTP_202_ACCEPTED)
    return redirect('https://web.remem.me/register-error')
    # return response.Response({'messages': [_('User activation failed')]}, status.HTTP_406_NOT_ACCEPTABLE)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.AllowAny])
def reset_password(request):
    serializer = PasswordResetSerializer(data={**request.data})
    if serializer.is_valid():
        user = serializer.validated_data['user']
        password = serializer.validated_data['password']
        user.is_active = True
        user.set_password(password)
        user.save()
        if not user.accounts.exists():
            _create_default_account(user)
        return response.Response({'messages': [_('Password changed successfully.')]}, status.HTTP_200_OK)
    else:
        messages = serializer.errors
        return response.Response(messages, status.HTTP_400_BAD_REQUEST)


@decorators.api_view(["PUT"])
@decorators.permission_classes([permissions.IsAuthenticated])
def change_email(request):
    serializer = EmailChangeSerializer(data={**request.data}, context={'request': request})
    if serializer.is_valid():
        request.user.email = serializer.validated_data['email']
        request.user.save()
        return response.Response({'email': request.user.email}, status.HTTP_200_OK)
    else:
        messages = serializer.errors
        return response.Response(messages, status.HTTP_400_BAD_REQUEST)


@decorators.api_view(["PUT"])
@decorators.permission_classes([permissions.IsAuthenticated])
def delete_user(request):
    serializer = UserDeleteSerializer(data={**request.data}, context={'request': request})
    if serializer.is_valid():
        request.user.delete()
        return response.Response(None, status.HTTP_204_NO_CONTENT)
    else:
        messages = serializer.errors
        return response.Response(messages, status.HTTP_400_BAD_REQUEST)


def _create_default_account(user: User):
    base_name = user.email.split('@')[0]
    account_name = base_name
    index = 0
    while True:
        count = Account.objects.filter(name=account_name).count()
        if count == 0:
            break
        index += 1
        account_name = base_name + str(index)
    Account(id=IdGenerator.next(), user=user, name=account_name, language=user.language, lang_ref=user.language).save()

