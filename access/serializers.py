import json

import requests
from django.contrib.auth import get_user_model
from django.utils import translation
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer, PasswordField

from access.tokens import account_activation_token

User = get_user_model()


class UserCreateSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True, style={"input_type": "password"})
    email = serializers.EmailField(required=True)

    class Meta:
        model = User
        fields = [
            "email",
            "password",
            "ip_address",
        ]
        extra_kwargs = {"password": {"write_only": True}}

    def create(self, validated_data):
        """
        Create and return a new User instance, or update an existing inactive user.

        This method handles the creation of a new user or updates an existing
        inactive user, including password hashing and setting additional fields.

        Args:
            validated_data (dict): The validated data from the serializer.

        Returns:
            User: The created or updated User instance.

        Raises:
            serializers.ValidationError: If the email is already in use by an active user.
        """
        email = validated_data["email"].lower()
        password = validated_data["password"]
        ip_address = validated_data["ip_address"]

        # Check for existing user
        existing_user = User.objects.filter(email__iexact=email).first()

        if existing_user:
            if existing_user.is_active or existing_user.accounts.exists():
                raise serializers.ValidationError(
                    {"email": [_("This email address is already in use.")]}
                )
            user = existing_user
        else:
            user = User(email=email, is_active=False)

        # Update user fields
        user.ip_address = ip_address
        user.language = translation.get_language()
        user.set_password(password)
        user.save()

        return user


class PasswordForgotSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)

    def validate(self, attrs):
        data = super().validate(attrs)
        email = data['email']
        try:
            user = User.objects.get(email__iexact=email)
        except User.DoesNotExist:
            raise serializers.ValidationError({"email": _("User not found")})
        return user


class EmailChangeSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(write_only=True, required=True, style={"input_type": "password"})

    def validate(self, attrs):
        data = super().validate(attrs)
        email = data['email'].lower()
        password = data['password']
        user: User = self.context['request'].user
        if not user.check_password(password):
            raise serializers.ValidationError(
                {"password": _("Password incorrect.")})
        if email and User.objects.filter(email__iexact=email).exists():
            raise serializers.ValidationError(
                {"email": _("This email address is already in use.")})
        return {'email': email}


class UserDeleteSerializer(serializers.Serializer):
    password = serializers.CharField(write_only=True, required=True, style={"input_type": "password"})

    def validate(self, attrs):
        data = super().validate(attrs)
        password = data['password']
        user: User = self.context['request'].user
        if not user.check_password(password):
            raise serializers.ValidationError(
                {"password": _("Password incorrect.")})
        if user.accounts.filter(deleted=False).exists():
            raise serializers.ValidationError(
                {'messages': [_("A user with active accounts cannot be deleted. Delete every account first.")]})
        return {}


class PasswordResetSerializer(serializers.Serializer):
    uidb64 = serializers.CharField()
    token = serializers.CharField()
    password = PasswordField()

    def validate(self, attrs):
        data = super().validate(attrs)
        uidb64 = data['uidb64']
        token = data['token']
        password = data['password']
        try:
            uid = force_str(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            raise serializers.ValidationError({'messages': _('User not found')})
        if not account_activation_token.check_token(user, token) or not user.is_active and user.accounts.exists():
            raise serializers.ValidationError({'messages': _('You do not have permission to perform this action.')})
        return {'user': user, 'password': password}


class TokenObtainWithLegacyCheckSerializer(TokenObtainPairSerializer):
    # legacy_server = 'http://air.local:8001'  # development
    legacy_server = 'https://my.remem.me'  # production

    default_error_messages = {
        "no_active_account": _("No active user found with the given credentials.")
    }

    def validate(self, attrs):
        try:
            data = super().validate(attrs)
        except AuthenticationFailed as err:
            url = self.legacy_server + '/convert/'
            try:
                response = requests.request('POST', url, data=json.dumps(attrs))
                response.encoding = 'utf-8-sig'
                if response.status_code == 201:
                    data = super().validate(attrs)
                else:
                    raise err
            except:
                raise err
        return data
