��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  L   �     �  P   	  f   Z  �   �  
  �	    �
  *   �  ;   �     (  (   ?     h     w  "   �  !   �     �     �        3     	   N     X     _     p                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 En användare med aktiva konton kan inte raderas. Radera varje konto först. Aktivera användaråtkomst Aktiverings-e-post skickad till {email}. Var vänlig och kontrollera din inkorg. E-post för att återställa lösenordet skickad till {email}. Var vänlig och kontrollera din inkorg. Hej,

Tack för att du använder Remember Me. För att aktivera din registrering, klicka på länken nedan eller använd den i din webbläsare:
{link}
Om du inte har registrerat dig, bara ignorera detta e-postmeddelande.

Gud välsigne dig! Hej,

Tack för att du använder Remember Me. För att återställa ditt lösenord, klicka på länken nedan eller använd den i din webbläsare:
{link}
Om du inte har begärt att återställa ditt lösenord, bara ignorera detta e-postmeddelande.

Gud välsigne dig! Hej,

Din senaste aktivitet på remem.me var för %(days)d dagar sedan.
Om du inte besöker remem.me på 120 dagar kommer ditt konto att bli avstängt. Du kan ansluta till ditt konto genom att logga in i Remember Me-appen eller på {link}.

Gud välsigne dig! Länk för att återställa ditt lösenord Ingen aktiv användare hittades med de angivna uppgifterna. Ingen giltig IP-adress Lösenordet har ändrats framgångsrikt. Fel lösenord. Återställ lösenord Denna e-postadress används redan. För många öppna registreringar Aktivering av användare Användarinaktivitet Användaren hittades inte Du har inte behörighet att utföra denna åtgärd. IP-adress Språk Senaste åtkomst Senaste meddelande 