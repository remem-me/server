msgid ""
msgstr ""
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-07 07:50+0200\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: mails.py:18
msgid "User Activation"
msgstr "Aktivering av användare"

#: mails.py:20
#, python-brace-format
#| msgid ""
#| "Hello,\n"
#| "\n"
#| "Thank you for using Remember Me. To activate your registration, please "
#| "follow the link below:\n"
#| "{link}\n"
#| "If you haven't registered, then just ignore this email.\n"
#| "\n"
#| "God bless!"
msgid ""
"Hello,\n"
"\n"
"Thank you for using Remember Me. To activate your registration, please click "
"on the link below or use it in your browser:\n"
"{link}\n"
"If you haven't registered, then just ignore this email.\n"
"\n"
"God bless!"
msgstr ""
"Hej,\n"
"\n"
"Tack för att du använder Remember Me. För att aktivera din registrering, klicka på länken nedan eller använd den i din webbläsare:\n{link}\nOm du inte har registrerat dig, bara ignorera detta e-postmeddelande.\n"
"\n"
"Gud välsigne dig!"

#: mails.py:24
msgid "Activate User Access"
msgstr "Aktivera användaråtkomst"

#: mails.py:29
msgid "Link to Reset Your Password"
msgstr "Länk för att återställa ditt lösenord"

#: mails.py:30
#, python-brace-format
#| msgid ""
#| "Hello,\n"
#| "\n"
#| "Thank you for using Remember Me. To activate your registration, please "
#| "follow the link below:\n"
#| "{link}\n"
#| "If you haven't registered, then just ignore this email.\n"
#| "\n"
#| "God bless!"
msgid ""
"Hello,\n"
"\n"
"Thank you for using Remember Me. To reset your password, please click on the "
"link below or use it in your browser:\n"
"{link}\n"
"If you haven't requested to reset your password, then just ignore this "
"email.\n"
"\n"
"God bless!"
msgstr ""
"Hej,\n"
"\n"
"Tack för att du använder Remember Me. För att återställa ditt lösenord, klicka på länken nedan eller använd den i din webbläsare:\n{link}\nOm du inte har begärt att återställa ditt lösenord, bara ignorera detta e-postmeddelande.\n"
"\n"
"Gud välsigne dig!"

#: mails.py:34
msgid "Reset Password"
msgstr "Återställ lösenord"

#: mails.py:39
msgid "User Inactivity"
msgstr "Användarinaktivitet"

#: mails.py:40
#, python-format, python-brace-format
#| msgid ""
#| "Hello,\n"
#| "\n"
#| "Your last activity on remem.me was %(days)d days ago.\n"
#| "If you don't access remem.me for 120 days, your account will be "
#| "suspended. You can connect to your account by logging in to the Remember "
#| "Me app or on {link}.\n"
#| "\n"
#| "God bless!"
msgid ""
"Hello,\n"
"\n"
"Your last activity on Remember Me remem.me was %(days)d days ago.\n"
"If you don't access remem.me for another 120 days, your account will be "
"suspended. You can connect to your account by logging in to the Remember Me "
"app or on {link}.\n"
"\n"
"God bless!"
msgstr ""
"Hej,\n"
"\n"
"Din senaste aktivitet på remem.me var för %(days)d dagar sedan.\n"
"Om du inte besöker remem.me på 120 dagar kommer ditt konto att bli avstängt. "
"Du kan ansluta till ditt konto genom att logga in i Remember Me-appen eller "
"på {link}.\n"
"\n"
"Gud välsigne dig!"

#: models.py:13
msgid "ip address"
msgstr "IP-adress"

#: models.py:14
msgid "language"
msgstr "Språk"

#: models.py:15
msgid "last access"
msgstr "Senaste åtkomst"

#: models.py:16
msgid "last notice"
msgstr "Senaste meddelande"

#: serializers.py:36 serializers.py:70
msgid "This email address is already in use."
msgstr "Denna e-postadress används redan."

#: serializers.py:52 serializers.py:104
msgid "User not found"
msgstr "Användaren hittades inte"

#: serializers.py:67 serializers.py:83
msgid "Password incorrect."
msgstr "Fel lösenord."

#: serializers.py:86
msgid ""
"A user with active accounts cannot be deleted. Delete every account first."
msgstr ""
"En användare med aktiva konton kan inte raderas. Radera varje konto först."

#: serializers.py:106
msgid "You do not have permission to perform this action."
msgstr "Du har inte behörighet att utföra denna åtgärd."

#: serializers.py:115
msgid "No active user found with the given credentials."
msgstr "Ingen aktiv användare hittades med de angivna uppgifterna."

#: views.py:28
msgid "No valid ip address"
msgstr "Ingen giltig IP-adress"

#: views.py:30
msgid "Too many open registrations"
msgstr "För många öppna registreringar"

#: views.py:36
#, python-brace-format
msgid "Activation email sent to {email}. Please check your inbox."
msgstr ""
"Aktiverings-e-post skickad till {email}. Var vänlig och kontrollera din "
"inkorg."

#: views.py:46
#, python-brace-format
msgid ""
"Email for resetting the password sent to {email}. Please check your inbox."
msgstr ""
"E-post för att återställa lösenordet skickad till {email}. Var vänlig och "
"kontrollera din inkorg."

#: views.py:104
msgid "Password changed successfully."
msgstr "Lösenordet har ändrats framgångsrikt."

#, python-brace-format
#~ msgid ""
#~ "Hello,\n"
#~ "\n"
#~ "Thank you for using Remember Me. Somebody has requested to reset your "
#~ "password. If it was you, please follow the link below:\n"
#~ "{link}\n"
#~ "Otherwise just ignore this email.\n"
#~ "\n"
#~ "God bless!"
#~ msgstr ""
#~ "Hej,\n"
#~ "\n"
#~ "Tack för att du använder Remember Me. Någon har begärt att återställa "
#~ "ditt lösenord. Om det var du, följ länken nedan:\n"
#~ "{link}\n"
#~ "Annars, ignorera detta e-postmeddelande.\n"
#~ "\n"
#~ "Gud välsigne dig!"
