��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  �   �  8   e  �   �  �   "	  �  �	  �  �  �  �  5   a  x   �  )     +   :     f     �  \   �  C      )   D  /   n  ,   �  L   �          (     1  '   O                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Користувач з активними обліковими записами не може бути видалений. Спочатку видаліть всі облікові записи. Активація доступу користувача Лист активації відправлено на {email}. Будь ласка, перевірте свою скриньку. Лист для скидання пароля відправлено на {email}. Будь ласка, перевірте свою скриньку. Привіт,

Дякуємо за використання Пам'ятай мене. Для активації вашого облікового запису, будь ласка, натисніть на посилання нижче або використовуйте його в вашому браузері:
{link}
Якщо ви не зареєструвалися, просто проігноруйте цей лист електронної пошти.

Боже благослови! Привіт,

Дякуємо за використання Пам'ятай мене. Для скидання пароля, будь ласка, натисніть на посилання нижче або використовуйте його в вашому браузері:
{link}
Якщо ви не надсилали запит на скидання пароля, просто проігноруйте цей лист електронної пошти.

Боже благослови! Привіт,

Остання активність на remem.me була %(days)d днів тому.
Якщо ви не увійдете на remem.me протягом 120 днів, ваш обліковий запис буде призупинений. Ви можете увійти в свій обліковий запис, увійшовши в додаток Пам'ятай мене або за посиланням: {link}.

Боже благослови! Посилання на скидання пароля За наданими обліковими даними не знайдено активного користувача. Немає дійсної IP-адреси Пароль успішно змінено. Невірний пароль. Скидання пароля Ця адреса електронної пошти вже використовується. Занадто багато відкритих реєстрацій Активація користувача Неактивність користувача Користувача не знайдено У вас немає дозволу на виконання цієї дії. IP-адреса Мова Останній доступ Останнє повідомлення 