��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  ]   �     �  R     e   m    �  $  �	  �   	  &   �  Q        l     �     �     �  ,   �     �          1     G  8   _  
   �     �     �     �                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Ein Benutzer mit aktiven Konten kann nicht gelöscht werden. Löschen Sie zuerst jedes Konto. Benutzerzugriff aktivieren Aktivierungs-E-Mail an {email} gesendet. Bitte überprüfen Sie Ihren Posteingang. E-Mail zum Zurücksetzen des Passworts an {email} gesendet. Bitte überprüfen Sie Ihren Posteingang. Hallo,

Vielen Dank, dass Sie Remember Me nutzen. Um Ihre Registrierung zu aktivieren, klicken Sie bitte auf den unten stehenden Link oder verwenden Sie ihn in Ihrem Browser:
{link}
Wenn Sie sich nicht registriert haben, ignorieren Sie einfach diese E-Mail.

Gottes Segen! Hallo,

Vielen Dank, dass Sie Remember Me nutzen. Um Ihr Passwort zurückzusetzen, klicken Sie bitte auf den unten stehenden Link oder verwenden Sie ihn in Ihrem Browser:
{link}
Wenn Sie kein Zurücksetzen Ihres Passworts angefordert haben, ignorieren Sie einfach diese E-Mail.

Gottes Segen! Hallo,

Ihre letzte Aktivität auf remem.me war vor %(days)d Tagen.
Wenn Sie remem.me 120 Tage lang nicht nutzen, wird Ihr Konto gesperrt. Sie können sich in der Remember Me-App oder auf {link} in Ihr Konto einloggen.

Gottes Segen! Link zum Zurücksetzen Ihres Passworts Es wurde kein aktiver Benutzer mit den angegebenen Anmeldeinformationen gefunden. Keine gültige IP-Adresse Passwort erfolgreich geändert. Falsches Passwort. Passwort zurücksetzen Diese E-Mail-Adresse wird bereits verwendet. Zu viele offene Registrierungen Benutzeraktivierung Benutzer-Inaktivität Benutzer nicht gefunden Sie haben keine Berechtigung, diese Aktion auszuführen. IP-Adresse Sprache letzter Zugriff letzte Benachrichtigung 