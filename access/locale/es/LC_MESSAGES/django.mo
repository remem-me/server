��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  W   �     �  ^     o   r  �   �    �	  �   �
  &   �  J   �      0  "   Q     t     �  7   �     �     �          (  ,   >     k     y     �     �                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 No se puede eliminar un usuario con cuentas activas. Primero elimine todas las cuentas. Activar Acceso de Usuario Correo electrónico de activación enviado a {email}. Por favor, revise su bandeja de entrada. Correo electrónico para restablecer la contraseña enviado a {email}. Por favor, revise su bandeja de entrada. Hola,

Gracias por utilizar Remember Me. Para activar su registro, por favor haga clic en el enlace de abajo o úselo en su navegador:
{link}
Si no se ha registrado, simplemente ignore este correo electrónico.

Dios lo bendiga! Hola,

Gracias por utilizar Remember Me. Para restablecer su contraseña, por favor haga clic en el enlace de abajo o úselo en su navegador:
{link}
Si no ha solicitado restablecer su contraseña, simplemente ignore este correo electrónico.

Dios lo bendiga! Hola,

Su última actividad en remem.me fue hace %(days)d días.
Si no accede a remem.me durante 120 días, su cuenta será suspendida.Puede conectarse a su cuenta iniciando sesión en la aplicación Remember Me o en {link}.

Dios lo bendiga! Enlace para Restablecer su Contraseña No se ha encontrado un usuario activo con las credenciales proporcionadas. No hay una dirección IP válida Contraseña cambiada exitosamente. Contraseña incorrecta. Restablecer Contraseña Esta dirección de correo electrónico ya está en uso. Demasiados registros abiertos Activación de Usuario Inactividad de Usuario Usuario no encontrado No tiene permiso para realizar esta acción. Dirección IP Idioma Último Acceso Último Aviso 