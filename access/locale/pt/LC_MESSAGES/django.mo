��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  W   �     �  W     `   k  �   �  �   �	  �   �
     �  ?   �     �     �          '  *   7     b     }     �     �  4   �     �                                                            	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Um usuário com contas ativas não pode ser excluído. Exclua todas as contas primeiro. Ativar Acesso do Usuário E-mail de ativação enviado para {email}. Por favor, verifique a sua caixa de entrada. E-mail para redefinir a senha enviado para {email}. Por favor, verifique a sua caixa de entrada. Olá,

Obrigado por usar o Remember Me. Para ativar o seu registro, por favor clique no link abaixo ou use-o em seu navegador:
{link}
Se você não se registrou, então apenas ignore este e-mail.

Deus abençoe! Olá,

Obrigado por utilizar o Remember Me. Para redefinir sua senha, por favor clique no link abaixo ou use-o em seu navegador:
{link}
Se você não solicitou a redefinição de senha, simplesmente ignore este e-mail.

Deus abençoe! Olá,

A sua última atividade em remem.me foi há %(days)d dias.
Se você não acessar remem.me por 120 dias, a sua conta será suspensa. Você pode se conectar à sua conta fazendo login no aplicativo Remember Me ou em {link}.

Deus abençoe! Link para Redefinir Sua Senha Nenhum usuário ativo encontrado com as credenciais fornecidas. Nenhum endereço IP válido Senha alterada com sucesso. Senha incorreta. Redefinir Senha Este endereço de e-mail já está em uso. Muitos registros em aberto Ativação de Usuário Inatividade do Usuário Usuário não encontrado. Você não tem permissão para executar esta ação. Endereço IP Idioma Último Acesso Último Aviso 