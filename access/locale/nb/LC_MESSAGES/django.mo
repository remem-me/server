��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  G   �     �  :   �  M   :  �   �    w	  �   �
  )   v  =   �     �     �            (   3     \     {     �     �  7   �  
   �     �                                                       	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 En bruker med aktive kontoer kan ikke slettes. Slett hver konto først. Aktiver brukertilgang Aktiverings-e-post sendt til {email}. Sjekk innboksen din. E-post for tilbakestilling av passord sendt til {email}. Sjekk innboksen din. Hei,

Takk for at du bruker Remember Me. For å aktivere registreringen din, vennligst klikk på lenken nedenfor eller bruk den i nettleseren din:
{link}
Hvis du ikke har registrert deg, kan du bare ignorere denne e-posten.

Velsigne deg! Hei,

Takk for at du bruker Remember Me. For å tilbakestille passordet ditt, vennligst klikk på lenken nedenfor eller bruk den i nettleseren din:
{link}
Hvis du ikke har bedt om å tilbakestille passordet ditt, kan du bare ignorere denne e-posten.

Velsigne deg! Hei,

Din siste aktivitet på remem.me var for %(days)d dager siden.
Hvis du ikke bruker remem.me på 120 dager, vil kontoen din bli suspendert. Du kan koble til kontoen din ved å logge inn på Remember Me-appen eller på {link}.

Velsigne deg! Lenke for å tilbakestille passordet ditt Ingen aktive brukere ble funnet med de gitte legitimasjonene. Ingen gyldig IP-adresse Passord endret vellykket. Feil passord. Tilbakestill passord Denne e-postadressen er allerede i bruk. For mange åpne registreringer Brukeraktivering Brukerinaktivitet Brukeren ble ikke funnet Du har ikke tillatelse til å utføre denne handlingen. IP-adresse Språk Siste tilgang Siste melding 