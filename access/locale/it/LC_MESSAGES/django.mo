��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  Y   �     �  J     W   ^  �   �  �   �	  �   �
  $   �  8   �     �     �          .  &   D     k     �     �     �  /   �     �          	                                                  	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Un utente con account attivi non può essere cancellato. Prima elimina tutti gli account. Attiva l'accesso utente Email di attivazione inviata a {email}. Controlla la tua casella di posta. Email per reimpostare la password inviata a {email}. Controlla la tua casella di posta. Ciao,

Grazie per aver utilizzato Remember Me. Per attivare la registrazione, fai clic sul link sottostante o utilizzalo nel tuo browser:
{link}
Se non ti sei registrato, ignora semplicemente questa email.

Dio ti benedica! Ciao,

Grazie per aver utilizzato Remember Me. Per reimpostare la password, fai clic sul link sottostante o utilizzalo nel tuo browser:
{link}
Se non hai richiesto di reimpostare la password, ignora semplicemente questa email.

Dio ti benedica! Ciao,

La tua ultima attività su remem.me è stata %(days)d giorni fa.
Se non accedi a remem.me per 120 giorni, il tuo account sarà sospeso. Puoi accedere al tuo account effettuando il login nell'app Remember Me o su {link}.

Dio ti benedica! Link per reimpostare la tua Password Nessun utente attivo trovato con le credenziali fornite. Nessun indirizzo IP valido Password cambiata con successo. Password incorretta. Reimposta la password Questo indirizzo email è già in uso. Troppe registrazioni aperte Attivazione utente Inattività dell'utente Utente non trovato Non hai il permesso per eseguire questa azione. Indirizzo IP Lingua Ultimo accesso Ultimo avviso 