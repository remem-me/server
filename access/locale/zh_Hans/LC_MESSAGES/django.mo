��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  H   �     �  >   �  M   <  �   �  �   Q	    
     -  -   @     n     �     �     �  $   �     �     �     �       $        C     L     S     `                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 拥有活跃账户的用户不能被删除。请先删除所有账户。 激活用户访问 激活邮件已发送至 {email}。请查看你的收件箱。 重置密码的电子邮件已发送至 {email}。请查看你的收件箱。 你好，

感谢您使用记念我。要激活您的注册，请单击下面的链接或在浏览器中使用它：
{link}
如果您没有注册，请忽略此电子邮件。

愿上帝保佑你！ 你好，

感谢您使用记念我。要重设密码，请单击下面的链接或在浏览器中使用它：
{link}
如果您没有请求重设密码，请忽略此电子邮件。

愿上帝保佑你！ 你好，

你在 remem.me 上的上次活动距今已有 %(days)d 天。
如果你在 120 天内不登录 remem.me，你的账户将被暂停。你可以通过在记念我应用程序上登录或者在{link}上登录来重新激活你的账户。

愿上帝保佑你！ 重置密码链接 使用提供的凭据未找到活跃用户。 没有有效的IP地址 密码已成功更改。 密码不正确。 重置密码 该电子邮件地址已被使用。 注册太多 用户激活 用户不活跃 用户未找到 你没有执行此操作的权限。 IP地址 语言 最后访问 最后通知 