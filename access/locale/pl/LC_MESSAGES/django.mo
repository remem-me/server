��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  U   �     �  i     s   ~  �   �    �	    �
       M   9     �  %   �     �     �  $   �           7     N     j  0   �     �     �     �     �                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Nie można usunąć użytkownika z aktywnymi kontami. Najpierw usuń wszystkie konta. Aktywuj Dostęp Użytkownika Wiadomość aktywacyjna została wysłana na adres {email}. Proszę sprawdź swoją skrzynkę odbiorczą. Wiadomość z resetowaniem hasła została wysłana na adres {email}. Proszę sprawdź swoją skrzynkę odbiorczą. Witaj,

Dziękujemy za korzystanie z Remember Me. Aby aktywować swoje konto, proszę kliknąć poniższy link lub użyć go w przeglądarce:
{link}
Jeśli nie dokonałeś rejestracji, po prostu zignoruj tę wiadomość e-mail.

Niech Bóg błogosławi! Witaj,

Dziękujemy za korzystanie z Remember Me. Aby zresetować hasło, proszę kliknąć poniższy link lub użyć go w przeglądarce:
{link}
Jeśli nie złożyłeś prośby o zresetowanie hasła, po prostu zignoruj tę wiadomość e-mail.

Niech Bóg błogosławi! Witaj,

Twoja ostatnia aktywność na remem.me miała miejsce %(days)d dni temu.
Jeśli nie zalogujesz się na remem.me przez 120 dni, twoje konto zostanie zawieszone. Możesz zalogować się na swoje konto, logując się do aplikacji Remember Me lub na {link}.

Niech Bóg błogosławi! Link do Zresetowania Hasła Nie znaleziono aktywnego użytkownika o podanych danych uwierzytelniających. Brak prawidłowego adresu IP Hasło zostało pomyślnie zmienione. Nieprawidłowe hasło. Zresetuj Hasło Ten adres email jest już w użyciu. Zbyt wiele otwartych rejestracji Aktywacja Użytkownika Nieaktywność Użytkownika Użytkownik nie znaleziony Nie masz uprawnień do wykonania tej czynności. adres IP język ostatni dostęp ostatnia wiadomość 