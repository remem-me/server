��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  [   �     �  Q     ^   i     �  %  �	         &  :   D       #   �     �     �  4   �  "        8     L     d  5   �  
   �     �     �     �                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Un utilizator cu conturi active nu poate fi șters. Ștergeți mai întâi toate conturile. Activare acces utilizator Emailul de activare a fost trimis la {email}. Vă rugăm să verificați inboxul. Emailul pentru resetarea parolei a fost trimis la {email}. Vă rugăm să verificați inboxul. Salut,

Vă mulțumim că utilizați Remember Me. Pentru a vă activa înregistrarea, vă rugăm să faceți clic pe link-ul de mai jos sau să îl utilizați în browser-ul dvs.:
{link}
Dacă nu v-ați înregistrat, pur și simplu ignorați acest email.

Dumnezeu să vă binecuvânteze! Salut,

Vă mulțumim că utilizați Remember Me. Pentru a vă reseta parola, vă rugăm să faceți clic pe link-ul de mai jos sau să îl utilizați în browser-ul dvs.:
{link}
Dacă nu ați solicitat resetarea parolei, pur și simplu ignorați acest email.

Dumnezeu să vă binecuvânteze! Salut,

Ultima dvs. activitate pe remem.me a fost acum %(days)d zile.
Dacă nu accesați remem.me timp de 120 de zile, contul dvs. va fi suspendat. Puteți vă conecta la contul dvs. autentificându-vă în aplicația Remember Me sau la {link}.

Dumnezeu să vă binecuvânteze! Link pentru resetarea parolei Nu s-a găsit niciun utilizator activ cu datele introduse. Nicio adresă IP validă Parola a fost schimbată cu succes. Parolă incorectă Resetare parolă Această adresă de email este deja în folosință. Prea multe înregistrări deschise Activare utilizator Inactivitate utilizator Utilizatorul nu a fost găsit Nu aveți permisiunea de a efectua această acțiune. adresă IP limbă ultimul acces ultima notificare 