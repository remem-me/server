��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  �   �  A   6  �   x  �   	  j  �	  j    �  w  0     l   L  %   �  <   �       "   7  >   Z  ?   �  .   �  '     /   0  T   `     �     �     �  !   �                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Потребител с активни акаунти не може да бъде изтрит. Първо изтрийте всеки акаунт. Активиране на потребителски достъп Изпратен е активационен имейл до {email}. Моля, проверете вашия пощенски кутия. Изпратен е имейл за нулиране на паролата до {email}. Моля, проверете вашия пощенски кутия. Здравейте,

Благодарим ви, че използвате Запомни ме. За да активирате регистрацията си, моля, следвайте линка по-долу:
{link}
Ако все още не сте се регистрирали, просто игнорирайте този имейл.

Благослов! Здравейте,

Благодарим ви, че използвате Запомни ме. За да активирате регистрацията си, моля, следвайте линка по-долу:
{link}
Ако все още не сте се регистрирали, просто игнорирайте този имейл.

Благослов! Здравейте,

Последната ви активност на remem.me беше преди %(days)d дни.
Ако не влезете в remem.me в рамките на 120 дни, вашият акаунт ще бъде спрян. Можете да се свържете с акаунта си, като влезете в приложението Запомни ме или на {link}.

Благослов! Линк за нулиране на парола Не беше намерен активен потребител с предоставените данни. Няма валиден IP адрес Паролата беше успешно променена. Грешна парола. Нулиране на парола Този имейл адрес вече се използва. Твърде много отворени регистрации Активиране на потребител Неактивен потребител Потребителят не е намерен Нямате разрешение да извършите това действие. IP адрес език последен достъп последно известие 