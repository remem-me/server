��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  c   �  (     k   .     �    	  !  '
    I  $   c  I   �     �  $   �             )   :     d     �     �     �  1   �                    /                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Az aktív fiókokkal rendelkező felhasználó nem törölhető. Először törölj minden fiókot. Felhasználó hozzáférés aktiválása Az aktiváló e-mail elküldve a következő címre: {email}. Kérjük, ellenőrizd a beérkező leveleket. A jelszó visszaállításáról szóló e-mailt elküldtük a(z) {email} címre. Kérjük, ellenőrizd a beérkező leveleket. Szia,

Köszönjük, hogy a Remember Me-t használja. Az regisztráció aktiválásához kérjük, kattintson az alábbi hivatkozásra vagy használja a böngészőjében:
{link}
Ha nem regisztrált, egyszerűen hagyja figyelmen kívül ezt az e-mailt.

Isten áldjon! Szia,

Köszönjük, hogy a Remember Me-t használja. A jelszó visszaállításához kérjük, kattintson az alábbi hivatkozásra vagy használja a böngészőjében:
{link}
Ha nem kérte a jelszó visszaállítását, egyszerűen hagyja figyelmen kívül ezt az e-mailt.

Isten áldjon! Szia,

Az utolsó tevékenységed a remem.me webhelyen %(days)d napja volt.
Ha 120 napig nem látogatod meg a Remember.me webhelyet, a fiókodat felfüggesztjük. Fiókodhoz úgy csatlakozhatsz, hogy bejelentkezel a Remember Me alkalmazásba, vagy pedig itt {link}.

Isten áldjon! Link a jelszó visszaállításához Nem található aktív felhasználó a megadott hitelesítési adatokkal. Érvénytelen IP-cím A jelszavad sikeresen megváltozott. Helytelen jelszó. Jelszó visszaállítása Ez az e-mail cím már használatban van. Túl sok nyitott regisztráció Felhasználói aktiválás Felhasználói inaktivitás Felhasználó nem található Nincs engedélyed ezt a műveletet végrehajtani. IP-cím Nyelv Utolsó hozzáférés Utolsó értesítés 