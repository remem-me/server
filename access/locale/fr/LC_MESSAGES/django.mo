��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  h   �  !   
  V   ,  m   �  �   �    �	       +   )  ?   U     �  "   �     �     �  *        3     P     l     �  7   �  
   �     �     �     �                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Un utilisateur ayant des comptes actifs ne peut pas être supprimé. Supprimez d'abord tous les comptes. Activer l'accès de l'utilisateur E-mail d'activation envoyé à {email}. Veuillez vérifier votre boîte de réception. E-mail pour réinitialiser le mot de passe envoyé à {email}. Veuillez vérifier votre boîte de réception. Bonjour,

Merci d'utiliser Remember Me. Pour activer votre inscription, veuillez cliquer sur le lien ci-dessous ou l'utiliser dans votre navigateur :
{link}
Si vous ne vous êtes pas inscrit, ignorez simplement cet e-mail.

Que Dieu vous bénisse! Bonjour,

Merci d'utiliser Remember Me. Pour réinitialiser votre mot de passe, veuillez cliquer sur le lien ci-dessous ou l'utiliser dans votre navigateur :
{link}
Si vous n'avez pas demandé à réinitialiser votre mot de passe, ignorez simplement cet e-mail.

Que Dieu vous bénisse! Bonjour,

Votre dernière activité sur remem.me remonte à %(days)d jours.
Si vous n'accédez pas à remem.me pendant 120 jours, votre compte sera suspendu. Vous pouvez vous connecter à votre compte en vous connectant à l'application Remember Me ou sur {link}.

Que Dieu vous bénisse! Lien pour réinitialiser votre mot de passe Aucun utilisateur actif trouvé avec les informations fournies. Aucune adresse IP valide Mot de passe changé avec succès. Mot de passe incorrect. Réinitialiser le mot de passe Cette adresse e-mail est déjà utilisée. Trop d'inscriptions en cours Activation de l'utilisateur Inactivité de l'utilisateur Utilisateur introuvable Vous n'avez pas la permission d'effectuer cette action. Adresse IP Langue Dernier accès Dernière notification 