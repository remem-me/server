ή          ά   %         0  J   1     |  :     J   Μ  Μ     Ϋ   δ  ϋ   ΐ     Ό  0   Ψ     	          <     P  %   _          ‘     ±     Α  2   Π  
                  #  q   /  o   ‘       ]   /  m     =  ϋ  Q  9
  /       »  J   Ω  $   $  6   I           ‘  3   Έ  /   μ          0  %   D  /   j  	        €     «     Ή                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 νμ±νλ κ³μ μ΄ μλ μ¬μ©μλ μ­μ ν  μ μμ΅λλ€. λ¨Όμ  λͺ¨λ  κ³μ μ μ­μ νμΈμ. μ¬μ©μ μ‘μΈμ€ νμ±ν νμ±ν μ΄λ©μΌμ΄ {email}λ‘ μ μ‘λμμ΅λλ€. λ°μ νΈμ§ν¨μ νμΈνμΈμ. λΉλ°λ²νΈ μ¬μ€μ μ© μ΄λ©μΌμ΄ {email}λ‘ μ μ‘λμμ΅λλ€. λ°μ νΈμ§ν¨μ νμΈνμΈμ. μλνμΈμ,

λ¦¬λ©€λ² λ―Έλ₯Ό μ¬μ©ν΄ μ£Όμμ κ°μ¬ν©λλ€. νμκ°μμ νμ±ννλ €λ©΄ μλ λ§ν¬λ₯Ό ν΄λ¦­νκ±°λ λΈλΌμ°μ μμ μ¬μ©νμΈμ:
{link}
νμκ°μνμ§ μμΌμ¨λ€λ©΄, μ΄ μ΄λ©μΌμ λ¬΄μνμλ©΄ λ©λλ€.

νλλμ μΆλ³΅μ΄ ν¨κ» νμκΈΈ λ°λλλ€! μλνμΈμ,

λ¦¬λ©€λ² λ―Έλ₯Ό μ¬μ©ν΄ μ£Όμμ κ°μ¬ν©λλ€. λΉλ°λ²νΈλ₯Ό μ¬μ€μ νλ €λ©΄ μλ λ§ν¬λ₯Ό ν΄λ¦­νκ±°λ λΈλΌμ°μ μμ μ¬μ©νμΈμ:
{link}
λΉλ°λ²νΈ μ¬μ€μ μ μμ²­νμ§ μμΌμ¨λ€λ©΄, μ΄ μ΄λ©μΌμ λ¬΄μνμλ©΄ λ©λλ€.

νλλμ μΆλ³΅μ΄ ν¨κ» νμκΈΈ λ°λλλ€! μλνμΈμ,

remem.meμμμ λ§μ§λ§ νλμ %(days)dμΌ μ μλλ€.
120μΌ λμ remem.meμ μ μνμ§ μμΌλ©΄ κ³μ μ΄ μΌμ μ€λ¨λ©λλ€. λ¦¬λ©€λ² μ±μ΄λ {link}μ λ‘κ·ΈμΈνμ¬ κ³μ μ μ°κ²°ν  μ μμ΅λλ€.

νλλμ μΆλ³΅μ΄ ν¨κ» νμκΈΈ λ°λλλ€! λΉλ°λ²νΈ μ¬μ€μ  λ§ν¬ μ£Όμ΄μ§ μκ²© μ¦λͺμΌλ‘ νμ± μ¬μ©μλ₯Ό μ°Ύμ μ μμ΅λλ€. μ ν¨ν IP μ£Όμκ° μμ΅λλ€. λΉλ°λ²νΈκ° μ±κ³΅μ μΌλ‘ λ³κ²½λμμ΅λλ€. λΉλ°λ²νΈκ° νλ Έμ΅λλ€. λΉλ°λ²νΈ μ¬μ€μ  μ΄ μ΄λ©μΌ μ£Όμλ μ΄λ―Έ μ¬μ© μ€μλλ€. λλ¬΄ λ§μ κ°λ°©λ λ±λ‘μ΄ μμ΅λλ€. μ¬μ©μ νμ±ν μ¬μ©μ λΉνλ μ¬μ©μλ₯Ό μ°Ύμ μ μμ΅λλ€. μ΄ μμμ μνν  κΆνμ΄ μμ΅λλ€. IP μ£Όμ μΈμ΄ μ΅μ’ μ μ μ΅κ·Ό κ³΅μ§ 