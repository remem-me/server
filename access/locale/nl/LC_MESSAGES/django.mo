��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  [   �     �  H     g   a  �   �  �   �	  �   �
  !   �  C   �          !     A     U  !   i      �     �     �     �  5   �     &     /     4     D                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Een gebruiker met actieve accounts kan niet worden verwijderd. Verwijder eerst elk account. Activeer gebruikerstoegang Activeringsmail verzonden naar {email}. Controleer alstublieft uw inbox. E-mail voor het resetten van het wachtwoord is verzonden naar {email}. Controleer alstublieft uw inbox. Hallo,

Bedankt voor het gebruik van Remember Me. Klik op de onderstaande link of gebruik deze in uw browser om uw registratie te activeren:
{link}
Als u zich niet heeft geregistreerd, negeer dan gewoon deze e-mail.

God zegene! Hallo,

Bedankt voor het gebruik van Remember Me. Klik op de onderstaande link of gebruik deze in uw browser om uw wachtwoord opnieuw in te stellen:
{link}
Als u uw wachtwoord niet opnieuw heeft aangevraagd, negeer dan gewoon deze e-mail.

God zegene! Hallo,

Uw laatste activiteit op remem.me was %(days)d dagen geleden.
Als u remem.me niet binnen 120 dagen gebruikt, wordt uw account opgeschort. U kunt verbinding maken met uw account door in te loggen op de Remember Me-app of op {link}.

God zegene! Link om uw wachtwoord te resetten Er is geen actieve gebruiker gevonden met de opgegeven referenties. Geen geldig IP-adres Wachtwoord succesvol gewijzigd. Wachtwoord onjuist. Wachtwoord resetten Dit e-mailadres is al in gebruik. Te veel openstaande registraties Gebruikersactivering Gebruikersinactiviteit Gebruiker niet gevonden U heeft geen toestemming om deze actie uit te voeren. IP-adres taal laatste toegang laatste kennisgeving 