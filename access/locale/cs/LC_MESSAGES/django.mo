��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  V   �     �  O     W   g  �   �  �   �	    �
     �  >   �     	  !        ?     R  ,   b  (   �     �     �     �  (   �  	        )     /     C                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Uživatel s aktivními účty nemůže být smazán. Nejprve vymažte všechny účty. Aktivovat přístup uživatele Aktivační e-mail byl odeslán na adresu {email}. Zkontrolujte svou schránku. E-mail pro obnovení hesla byl odeslán na adresu {email}. Zkontrolujte svou schránku. Ahoj,

Děkujeme vám za použití Remember Me. Pro aktivaci svého účtu klikněte prosím na odkaz níže nebo ho použijte ve vašem prohlížeči:
{link}
Pokud jste se neregistrovali, jednoduše ignorujte tento e-mail.

Bůh vás žehnej! Ahoj,

Děkujeme vám za použití Remember Me. Pro obnovení hesla klikněte prosím na odkaz níže nebo ho použijte ve vašem prohlížeči:
{link}
Pokud jste nepožádali o obnovení hesla, jednoduše ignorujte tento e-mail.

Bůh vás žehnej! Ahoj,

Vaše poslední aktivita na remem.me byla před %(days)d dny.
Pokud se do 120 dnů nepřihlásíte na remem.me, bude váš účet pozastaven. Přihlásit se můžete do svého účtu přes aplikaci Remember Me nebo na odkazu {link}.

Bůh vás žehnej! Odkaz na obnovení hesla Nebyl nalezen žádný aktivní uživatel s uvedenými údaji. Neplatná IP adresa Heslo bylo úspěšně změněno. Nesprávné heslo. Obnovení hesla Tato e-mailová adresa je již používána. Příliš mnoho otevřených registrací Aktivace uživatele Nečinnost uživatele Uživatel nenalezen Nemáte oprávnění provést tuto akci. IP adresa Jazyk Poslední přístup Poslední oznámení 