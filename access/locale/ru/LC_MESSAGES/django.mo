��          �   %   �      0  J   1     |  :   �  J   �  �     �   �  �   �     �  0   �     	          <     P  %   _     �     �     �     �  2   �  
                  #  q   /  �   �  >   C  �   �  �   	  �  �	  �  [  �    -   �  w   �  5   X  +   �     �     �  V   �  A   H  +   �  1   �  *   �  P        d     r     {  )   �                                             	                             
                                                     A user with active accounts cannot be deleted. Delete every account first. Activate User Access Activation email sent to {email}. Please check your inbox. Email for resetting the password sent to {email}. Please check your inbox. Hello,

Thank you for using Remember Me. To activate your registration, please click on the link below or use it in your browser:
{link}
If you haven't registered, then just ignore this email.

God bless! Hello,

Thank you for using Remember Me. To reset your password, please click on the link below or use it in your browser:
{link}
If you haven't requested to reset your password, then just ignore this email.

God bless! Hello,

Your last activity on Remember Me remem.me was %(days)d days ago.
If you don't access remem.me for another 120 days, your account will be suspended. You can connect to your account by logging in to the Remember Me app or on {link}.

God bless! Link to Reset Your Password No active user found with the given credentials. No valid ip address Password changed successfully. Password incorrect. Reset Password This email address is already in use. Too many open registrations User Activation User Inactivity User not found You do not have permission to perform this action. ip address language last access last notice Report-Msgid-Bugs-To: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Пользователь с активными аккаунтами не может быть удален. Сначала удалите все аккаунты. Активировать доступ пользователя Активационное письмо отправлено на {email}. Пожалуйста, проверьте вашу почту. Письмо для сброса пароля отправлено на {email}. Пожалуйста, проверьте вашу почту. Здравствуйте,

Спасибо за использование Помни меня. Для активации вашей учетной записи, пожалуйста, нажмите на ссылку ниже или используйте ее в вашем браузере:
{link}
Если вы не зарегистрировались, просто проигнорируйте это письмо.

Благослови вас! Здравствуйте,

Спасибо за использование Помни меня. Для сброса пароля, пожалуйста, нажмите на ссылку ниже или используйте ее в вашем браузере:
{link}
Если вы не запрашивали сброс пароля, просто проигнорируйте это письмо.

Благослови вас! Здравствуйте,

Ваша последняя активность на remem.me была %(days)d дней назад.
Если вы не войдете в remem.me в течение 120 дней, ваш аккаунт будет заблокирован. Вы можете войти в свой аккаунт, войдя в приложение Помни меня или на {link}.

Благослови вас! Ссылка для сброса пароля С активными пользователями с такими учетными данными не найдено. Нет действительного IP-адреса Пароль успешно изменен. Неверный пароль. Сброс пароля Этот адрес электронной почты уже используется. Слишком много открытых регистраций Активация пользователя Неактивность пользователя Пользователь не найден У вас нет прав на выполнение этого действия. IP-адрес Язык Последний доступ Последнее уведомление 