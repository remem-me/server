from cuser.models import AbstractCUser, CUserManager, CUser  # package: django-username-email
from django.db import models
from django.utils.translation import gettext_lazy as _


class CaseInsensitiveUserManager(CUserManager):
    def get_by_natural_key(self, username):
        return self.get(**{self.model.USERNAME_FIELD + '__iexact': username})


class User(AbstractCUser):
    objects = CaseInsensitiveUserManager()
    ip_address = models.CharField(_('ip address'), max_length=40)
    language = models.CharField(_('language'), max_length=15, null=True, blank=True)
    last_access = models.DateField(_('last access'), null=True, blank=True)
    last_notice = models.DateField(_('last notice'), null=True, blank=True)

