from django.core import mail
from rest_framework import status
from rest_framework.test import APITestCase, APIRequestFactory

from access.models import User
from access.views import registration, forgot_password
from main.models import Account


class AccessViewsTestCase(APITestCase):
    def test_registration_success(self):
        factory = APIRequestFactory()
        request = factory.post('/v1/access/register/', {'email': 'test@mail.com', 'password': 'secret'}, format='json')
        response = registration(request)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual('Activation email sent to test@mail.com. Please check your inbox.',
                         response.data['messages'][0], 'test@mail.com')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual('User Activation', mail.outbox[0].subject)
        self.assertRegex(str(mail.outbox[0].body), r'https://testserver/v1/access/activate/\w+/\w+-\w+')
        self.assertFalse(User.objects.get(email='test@mail.com').is_active)

    def test_registration_already_exists(self):
        User.objects.create(email='test@mail.com', password='secret')
        factory = APIRequestFactory()
        request = factory.post('/v1/access/register/', {'email': 'test@mail.com', 'password': 'secret'}, format='json')
        response = registration(request)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertEqual('This email address is already in use.',
                         str(response.data['email'][0]),
                         'test@mail.com')
        self.assertEqual(len(mail.outbox), 0)

    def test_inactive_registration_get_overwritten(self):
        User.objects.create(email='test@mail.com', password='secret', is_active=False)
        factory = APIRequestFactory()
        request = factory.post('/v1/access/register/', {'email': 'test@mail.com', 'password': 'secret'}, format='json')
        response = registration(request)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual('Activation email sent to test@mail.com. Please check your inbox.',
                         response.data['messages'][0], 'test@mail.com')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual('User Activation', mail.outbox[0].subject)
        self.assertRegex(str(mail.outbox[0].body), r'https://testserver/v1/access/activate/\w+/\w+-\w+')
        self.assertFalse(User.objects.get(email='test@mail.com').is_active)

    def test_inactive_registration_with_accounts_already_exists(self):
        user = User.objects.create(email='test@mail.com', password='secret', is_active=False)
        Account.objects.create(id=1, user=user, modified=0, name="my account")
        factory = APIRequestFactory()
        request = factory.post('/v1/access/register/', {'email': 'test@mail.com', 'password': 'secret'}, format='json')
        response = registration(request)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertEqual('This email address is already in use.',
                         str(response.data['email'][0]),
                         'test@mail.com')
        self.assertEqual(len(mail.outbox), 0)

    def test_forgot_password(self):
        User.objects.create(email='test@mail.com', password='secret')
        factory = APIRequestFactory()
        request = factory.post('/v1/access/forgot-password/', {'email': 'test@mail.com'}, format='json')
        response = forgot_password(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual('Email for resetting the password sent to test@mail.com. Please check your inbox.',
                         response.data['messages'][0], 'test@mail.com')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual('Link to Reset Your Password', mail.outbox[0].subject)
        self.assertRegex(str(mail.outbox[0].body), r'https://web.remem.me/reset-password\?uidb64=\w+&token=\w+-\w+')
        self.assertTrue(User.objects.get(email='test@mail.com').is_active)

    def test_forgot_password_not_exists(self):
        factory = APIRequestFactory()
        request = factory.post('/v1/access/forgot-password/', {'email': 'test@mail.com'}, format='json')
        response = forgot_password(request)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
        self.assertEqual('User not found',
                         str(response.data['email'][0]),
                         'test@mail.com')
        self.assertEqual(len(mail.outbox), 0)
