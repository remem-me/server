import datetime

from django.utils import translation

from access.serializers import User


def _update_last_access(user: User):
    today = datetime.date.today()
    language = translation.get_language()
    is_updated = False
    if user.last_access is None or user.last_access < today:
        user.last_access = today
        if user.last_notice is not None:
            user.last_notice = None
        is_updated = True
    if user.language != language:
        user.language = language
        is_updated = True
    if is_updated:
        user.save()


class UpdateLastAccessMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.
        if request.user is not None and request.user.is_authenticated:
            _update_last_access(request.user)

        return response
