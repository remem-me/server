import datetime

from django.core.management import BaseCommand

from access.models import User


class Command(BaseCommand):
    help = 'Removes stale user registrations' \
           'Usage: python $HOME/rm_replication/manage.py clearregistrations 3 --settings=rm_replication.settings.prod'

    def add_arguments(self, parser):
        parser.add_argument('days', type=int)  # for list add nargs='+' or nargs='3' (length must be 3)

    def handle(self, *args, **options):
        days = int(options['days'])
        today = datetime.date.today()
        date = today - datetime.timedelta(days=days)

        # delete never activated users
        for user in User.objects.filter(is_active=False, last_access__isnull=True, last_notice__isnull=True,
                                        date_joined__lte=date):
            if not user.accounts.exists():
                self.stdout.write(self.style.SUCCESS(
                    'User %s deleted after %d days not activated.' % (
                        user.email, (today - user.date_joined.date()).days)))
                user.delete()
