import datetime

from django.core.management import BaseCommand
from django.utils import translation

from access.mails import send_inactivity_notice
from access.models import User


class Command(BaseCommand):
    help = 'Notifies, deactivates and removes inactive users. ' \
           'Usage: python $HOME/rm_replication/manage.py deactivateusers 120 --settings=rm_replication.settings.prod'

    def add_arguments(self, parser):
        parser.add_argument('days', type=int)  # for list add nargs='+' or nargs='3' (length must be 3)

    def handle(self, *args, **options):
        days = int(options['days'])
        today = datetime.date.today()
        date = today - datetime.timedelta(days=days)

        # notify users
        for user in User.objects.filter(last_access__lte=date, is_active=True, last_notice__isnull=True):
            Command.activate_language(user)
            send_inactivity_notice(user, (today - user.last_access).days)
            user.last_notice = today
            user.save()

        # deactivate notified users
        for user in User.objects.filter(last_notice__lte=date, is_active=True):
            user.is_active = False
            user.last_notice = today
            # user.save()
            self.stdout.write(self.style.SUCCESS(
                'User %s deactivated after more than %d days after last notice.' % (user.email, days)))

        # delete deactivated users
        for user in User.objects.filter(last_notice__lte=date, is_active=False):
            self.stdout.write(self.style.SUCCESS(
                'User %s deleted after more than %d days after deactivation.' % (user.email, days)))
            # user.delete()

    @staticmethod
    def activate_language(user):
        language = user.language
        if not language:
            account = user.accounts.first()
            if account and translation.check_for_language(account.lang_ref):
                language = account.lang_ref
        if language and language != translation.get_language():
            translation.activate(language)
