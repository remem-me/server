# Remember Me Replication Server

Replication server for the Bible memorization app Remember Me

## Getting started

* Check out [the docs][docs].

## Need help?

Please use the [user forum][user forum] for help requests and how-to questions.
Please open Gitlab issues for bugs and enhancements only, not general help requests.

## Want to contribute?

If you want to contribute through code or documentation, the [Contributing
guide is the best place to start][contributing]. If you have questions, feel free
to ask.

## Want to support us?

If you want to support us financially, you can [help fund the project
through a Paypal donation][paypal]. 


[docs]: http://www.remem.me
[user forum]: https://groups.google.com/forum/#!forum/rememberapp
[contributing]: https://gitlab.com/remem-me/server/tree/master/CONTRIBUTING.md
[paypal]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=NHMVF5RBNH3TE



