from io import StringIO

from django.utils.encoding import force_str
from django.utils.xmlutils import SimplerXMLGenerator
from rest_framework.renderers import BaseRenderer


class RootAttrXMLRenderer(BaseRenderer):
    """
    Renderer which serializes to XML and accepts root attributes.
    """
    media_type = "application/xml"
    format = "xml"
    charset = "utf-8"
    item_tag_name = "list-item"
    root_tag_name = "root"
    root_attributes = {}
    sub_root_tag_name = None
    sub_root_attributes = {}
    sub_root_data = {}

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """
        Renders `data` into serialized XML.
        """
        if data is None:
            return ""

        stream = StringIO()

        xml = SimplerXMLGenerator(stream, self.charset)
        xml.startDocument()
        xml.startElement(self.root_tag_name, self.root_attributes)

        if self.sub_root_tag_name is not None:
            xml.startElement(self.sub_root_tag_name, self.sub_root_attributes)
            self._to_xml(xml, self.sub_root_data)

        self._to_xml(xml, data)

        if self.sub_root_tag_name is not None:
            xml.endElement(self.sub_root_tag_name)

        xml.endElement(self.root_tag_name)
        xml.endDocument()
        return stream.getvalue()

    def _to_xml(self, xml, data):
        if isinstance(data, (list, tuple)):
            for item in data:
                xml.startElement(self.item_tag_name, {})
                self._to_xml(xml, item)
                xml.endElement(self.item_tag_name)

        elif isinstance(data, dict):
            for key, value in data.items():
                if key != 'attributes':
                    xml.startElement(key, value.get('attributes', {}) if isinstance(value, dict) else {})
                    self._to_xml(xml, value)
                    xml.endElement(key)

        elif data is None:
            # Don't output any value
            pass

        else:
            xml.characters(force_str(data))


class UrlXmlRenderer(RootAttrXMLRenderer):
    root_tag_name = 'urlset'
    item_tag_name = 'url'
    root_attributes = {'xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9'}


class FeedXmlRenderer(RootAttrXMLRenderer):
    root_tag_name = 'rss'
    sub_root_tag_name = 'channel'
    item_tag_name = 'item'
    root_attributes = {'version': '2.0'}
    sub_root_data = {'title': 'Bible Verse Collections | Remember Me. Bible Memory Joy',
                     'description': 'Bible verse collections for meditation, memorization, and Bible study',
                     'link': 'https://web.remem.me/collections',
                     'image': {'url': 'https://www.remem.me/images/ad1024x500.jpeg',
                               'title': 'Bible Verse Collections | Remember Me. Bible Memory Joy',
                               'link': 'https://web.remem.me/collections'}}
