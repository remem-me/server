from django.urls import re_path
from rest_framework.routers import SimpleRouter

from main import views
from main.views import AccountViewSet, VerseViewSet, ScoreViewSet, DeckViewSet
from main.views import TagViewSet

app_name = 'main'
router = SimpleRouter()
router.register('accounts', AccountViewSet, basename='accounts')
router.register('tags', TagViewSet, basename='tags')
router.register('verses', VerseViewSet, basename='verses')
router.register('scores', ScoreViewSet, basename='scores')
router.register('decks', DeckViewSet, basename='decks')
urlpatterns = router.urls + [
    re_path('^collections/$', views.CollectionList.as_view()),
    re_path('^collections/sitemap.xml$', views.CollectionSitemap.as_view()),
    re_path('^collections/((?P<locale>[0-9A-Za-z-]+)/)?feed.xml$', views.CollectionFeed.as_view()),
    re_path('^collections/(?P<pk>[^/.]+)/$', views.CollectionView.as_view()),
]
