from django.contrib import admin
from django.db.models import Count
from django.urls import reverse
from django.utils.html import format_html

from main.models import *


# Register your models here.

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    search_fields = ['name', 'user__email']
    autocomplete_fields = ['user']
    list_filter = ('language', 'lang_ref')
    list_display = ('name', 'language', 'lang_ref')


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    search_fields = ['id', 'account__name', 'text']
    autocomplete_fields = ['account']


@admin.register(Deck)
class DeckAdmin(admin.ModelAdmin):
    search_fields = ['name', 'tag__account__name']
    autocomplete_fields = ['tag']
    filter_horizontal = ('importers',)
    list_filter = ('featured', 'tag__account__language')
    list_display = ('name', 'account', 'verse_count',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.annotate(_verse_count=Count("tag__verses", distinct=True), )
        return qs

    def verse_count(self, deck_instance):
        return deck_instance._verse_count

    def account(self, deck_instance):
        info = (Account._meta.app_label, Account._meta.model_name)
        url = reverse('admin:{}_{}_change'.format(*info), args=(deck_instance.tag.account.pk,))
        return format_html('<a href="{url}">{text}</a>'.format(
            url=url,
            text=deck_instance.tag.account.name))


@admin.register(Verse)
class VerseAdmin(admin.ModelAdmin):
    search_fields = ['reference']
    autocomplete_fields = ['tags', 'account']
    filter_horizontal = ('tags',)


@admin.register(Score)
class ScoreAdmin(admin.ModelAdmin):
    search_fields = ['account_name']
    autocomplete_fields = ['account']
