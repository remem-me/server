import logging
from datetime import date
from email.utils import format_datetime

from django.db import IntegrityError, transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.settings import api_settings
from rest_framework.utils import html

from .models import Account, Tag, Verse, Score, Deck, Accountable
from .util import now

logger = logging.getLogger('app')


class NodePrimaryKeyField(serializers.PrimaryKeyRelatedField):
    """
    Custom DRF serializer field for proper handling of
    Node Foreign Key by ListSerializer on validation error
    """

    def to_representation(self, value):
        """
        Return pk value of serialized Node object
        if available else return given ID value
        """
        if self.pk_field is not None:
            return self.pk_field.to_representation(value.pk)
        return getattr(value, 'pk', value)


class ReplicableListSerializer(serializers.ListSerializer):
    def update(self, instance, validated_data):
        # Perform creations and updates.
        result = []
        timestamp = now()
        for data in validated_data:
            data['modified_on_server'] = timestamp
            item = instance.filter(id=data['id']).first()
            try:
                with transaction.atomic():
                    if item is None:
                        result.append(self.child.create(data))
                    elif item.modified < data['modified']:
                        result.append(self.child.update(item, data))
            except IntegrityError as e:
                self.__log_item_error(data, e)
                existing = self.get_existing(data)
                if 'account' in data:
                    account = data['account']
                    if (account and len(existing) > 0 and
                            existing[0].account.id == account.id and
                            data['modified'] > existing[0].modified):
                        self.delete_existing(existing[0])
                        result.append(self.child.create(data))
                        message = '{serializer} {id} deleted and created.'.format(
                            serializer=self.child.__class__.__name__, id=data['id'])
                        logger.info(message)
        return result

    def get_existing(self, data):
        return self.child.Meta.model.objects.filter(id=data['id'])

    def delete_existing(self, existing):
        existing.delete()

    def log_conflict(self, message: str):
        logger.warning(message)

    def __log_item_error(self, data, e):
        message = '{serializer} not saved. REASON: {error}'.format(serializer=self.child.__class__.__name__, error=e)
        if 'account' in data:
            account = data['account']
            message += 'ACCOUNT: {id}, {name}'.format(id=account.id, name=account.name)
        message += '.\n'
        existing = self.child.Meta.model.objects.filter(id=data['id'])
        if len(existing) > 0:
            message += 'EXISTING ITEM: {id}'.format(id=existing[0].id)
            if isinstance(existing[0], Accountable) and existing[0].account:
                account = existing[0].account
                message += ' (account: {id}, {name})'.format(id=account.id, name=account.name)
            message += '.\n'
        self.log_conflict(message)

    def to_internal_value(self, data):
        """
        List of dicts of native values <- List of dicts of primitive datatypes.
        """
        if html.is_html_input(data):
            data = html.parse_html_list(data, default=[])

        if not isinstance(data, list):
            message = self.error_messages['not_a_list'].format(
                input_type=type(data).__name__
            )
            raise ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: [message]
            }, code='not_a_list')

        if not self.allow_empty and len(data) == 0:
            message = self.error_messages['empty']
            raise ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: [message]
            }, code='empty')

        ret = []
        errors = []

        for item in data:
            try:
                # custom code to prevent missing 'pk' error
                try:
                    self.child.instance = self.instance.get(id=item['id']) if self.instance else None
                except self.child.Meta.model.DoesNotExist:
                    self.child.instance = None
                self.child.initial_data = item
                # end of custom code
                validated = self.child.run_validation(item)
            except ValidationError as exc:
                errors.append(exc.detail)
            else:
                ret.append(validated)
                errors.append({})

        if any(errors):
            raise ValidationError(errors)

        return ret


class ScoreListSerializer(ReplicableListSerializer):
    def create(self, validated_data):
        result = super().create(validated_data)
        self.__purge_scores(result)
        return result

    def update(self, instance, validated_data):
        result = super().update(instance, validated_data)
        self.__purge_scores(result)
        return result

    def __purge_scores(self, result):
        if len(result) > 0:
            account = result[0].account
            if account.last_score_purge is None or account.last_score_purge < date.today():
                account.purge_scores()

    def get_existing(self, data):
        return self.child.Meta.model.objects.filter(vkey=data['vkey'], date=data['date'], deleted_id=0,
                                                    account=data['account'])

    def delete_existing(self, existing):
        existing.deleted_id = existing.id
        existing.save()

    def log_conflict(self, message: str):
        logger.debug(message)


class AccountSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        fields = ('id', 'modified', 'modified_by', 'deleted', 'name', 'language', 'lang_ref', 'review_limit',
                  'daily_goal', 'review_frequency', 'review_batch', 'inverse_limit',
                  'order_new', 'order_due', 'order_known', 'order_all',
                  'reference_included', 'topic_preferred', 'imported_decks', 'default_source', 'canon', 'font_type')
        model = Account
        list_serializer_class = ReplicableListSerializer

    def to_internal_value(self, data):
        # Remove non-existing decks from imported_decks before calling super
        if 'imported_decks' in data:
            data['imported_decks'] = [deck for deck in data['imported_decks'] if
                                      Deck.objects.filter(tag_id=deck).exists()]
        return super().to_internal_value(data)


class TagSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    published = serializers.BooleanField(read_only=True)

    class Meta:
        fields = ('id', 'modified', 'modified_by', 'deleted', 'account', 'text', 'included', 'published')
        model = Tag
        list_serializer_class = ReplicableListSerializer


class VerseSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        fields = (
            'id', 'modified', 'modified_by', 'deleted', 'account', 'tags', 'reference', 'source', 'topic', 'passage',
            'review', 'commit', 'level', 'image', 'note')
        model = Verse
        list_serializer_class = ReplicableListSerializer

    def to_internal_value(self, data):
        # Remove non-existing tags from tags before calling super
        if 'tags' in data:
            data['tags'] = [tag for tag in data['tags'] if
                                      Tag.objects.filter(id=tag).exists()]
        return super().to_internal_value(data)


class ScoreSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    account = NodePrimaryKeyField(queryset=Account.objects.all())
    deleted = serializers.BooleanField(default=False)
    deleted_id = serializers.IntegerField(default=0, write_only=True, required=False)

    def create(self, validated_data):
        validated_data['deleted_id'] = validated_data['id'] if validated_data.get('deleted') else 0
        return super().create(validated_data)

    def update(self, instance, validated_data):
        validated_data['deleted_id'] = validated_data['id'] if validated_data.get('deleted') else 0
        return super().update(instance, validated_data)

    class Meta:
        fields = ('id', 'modified', 'modified_by', 'deleted', 'deleted_id', 'account', 'date', 'vkey', 'change')
        model = Score
        list_serializer_class = ScoreListSerializer
        validators = []  # Remove a default UniqueTogetherValidator.
        # the default set of validators is defined in ModelSerializer.get_validators


class DeckSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('tag', 'name', 'description', 'website', 'image')
        model = Deck


class PublisherSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'name', 'language', 'lang_ref')
        model = Account


class CollectionSerializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(format='%Y-%m-%d')
    publisher = PublisherSerializer()
    downloads = serializers.IntegerField(read_only=True)
    size = serializers.IntegerField(read_only=True)

    class Meta:
        fields = (
            'id', 'label', 'name', 'description', 'website', 'created', 'size', 'downloads', 'publisher', 'safe_image')
        model = Deck


class CollectionVerseSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'reference', 'source', 'topic', 'passage', 'safe_image')
        model = Verse


class CollectionDetailSerializer(CollectionSerializer):
    verses = CollectionVerseSerializer(many=True, read_only=True)

    class Meta(CollectionSerializer.Meta):
        fields = CollectionSerializer.Meta.fields + ('verses',)


class CollectionUrlSerializer(serializers.ModelSerializer):
    loc = serializers.URLField()
    lastmod = serializers.DateTimeField(format='%Y-%m-%d')

    class Meta:
        fields = ('loc', 'lastmod')
        model = Deck


class CollectionFeedSerializer(serializers.ModelSerializer):
    link = serializers.SerializerMethodField()
    pubDate = serializers.SerializerMethodField('get_pub_date')
    title = serializers.SerializerMethodField()
    enclosure = serializers.SerializerMethodField()

    def get_link(self, obj: Deck):
        return obj.loc

    def get_pub_date(self, obj: Deck):
        return format_datetime(obj.created)

    def get_title(self, obj: Deck):
        return obj.name

    def get_enclosure(self, obj: Deck):
        return {'attributes': {'url': obj.safe_image, 'length': '0', 'type': 'image/jpeg'}}

    class Meta:
        fields = ('link', 'pubDate', 'title', 'description', 'enclosure')
        model = Deck
