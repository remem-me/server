from datetime import date, timedelta

from django.db import models
from django.db.models import When, Case
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from access.models import User
from main.util import now


# Future data model:
#
#                        Verse
#                       /  |
#                      1:n |
#                     /    |
# User----1:n----Account    n:m
# (Django auth)       \    |
#                      1:n |
#                       \  |
#                        Tag----1:?----Deck (language, lang_ref defined by Account)
#                -language (passage, tts, search)
#                -lang_ref (reference, tts, sorting, Bible query)
#                -schedule type: exponential, day->week->month (fixed/no_progression set in 'frequency', e.g. prayers)


class Accountable(models.Model):
    account = models.ForeignKey('Account', on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Replicable(models.Model):
    id = models.BigIntegerField(primary_key=True)
    modified = models.BigIntegerField(default=now)  # for synchronization
    modified_on_server = models.BigIntegerField(default=now)  # for synchronization
    modified_by = models.CharField(max_length=36, null=True, blank=True)  # for synchronization
    deleted = models.BooleanField(default=False)  # for synchronization and wastebin

    def save(self, *args, **kwargs):
        self.modified_on_server = now()
        super(Replicable, self).save(*args, **kwargs)

    class Meta:
        abstract = True


class Tag(Replicable, Accountable):
    text = models.CharField(max_length=200)
    included = models.BooleanField(null=True)

    @property
    def published(self) -> bool:
        return Deck.objects.filter(tag__id=self.id).exists()

    def __str__(self):
        return '%s (%s)' % (self.text, self.account)

    class Meta:
        constraints = [
            models.CheckConstraint(check=~models.Q(text=""), name="non_empty_text")
        ]
        indexes = [
            models.Index(fields=['account', 'text'], name='tag_text_idx'),
        ]
        ordering = ['text', 'account__name']


class Deck(models.Model):
    tag = models.OneToOneField('Tag', primary_key=True, on_delete=models.CASCADE, related_name='deck')
    importers = models.ManyToManyField('Account', related_name='imported_decks', blank=True)
    name = models.CharField(max_length=500)
    description = models.CharField(max_length=5000)
    created = models.DateTimeField(default=timezone.now)  # allows modification; see doc for 'auto_now_add'
    website = models.CharField(max_length=500, null=True, blank=True)
    image = models.CharField(max_length=1500, null=True, blank=True)
    featured = models.BooleanField(default=False)

    @property
    def id(self):
        return self.tag.id

    @property
    def publisher(self):
        return self.tag.account

    @property
    def label(self):
        return self.tag.text

    @property
    def verses(self):
        return self.tag.verses.filter(deleted=False)

    @property
    def loc(self):
        return f'https://web.remem.me/collections/{self.id}'

    @property
    def lastmod(self):
        return self.created

    @property
    def safe_image(self):
        if self.image and self.image.startswith('https://images.unsplash.com/'):
            return self.image
        else:
            verses_with_image = list(filter(lambda x: x.safe_image, self.tag.verses.all()))
            if len(verses_with_image) > 0:
                return verses_with_image[0].image
            else:
                return 'https://www.remem.me/images/ad1024x500.jpeg'

    def __str__(self):
        return '%s (%s, %s)' % (self.name, self.tag.text, self.tag.account)

    class Meta:
        constraints = [
            models.CheckConstraint(check=~models.Q(name=""), name="non_empty_name")
        ]
        indexes = [
            models.Index(fields=['tag'], name='deck_tag_idx'),
        ]
        ordering = ['name', 'tag__account__name']


class Account(Replicable):
    name = models.CharField(_('name'), max_length=200, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='accounts', null=False, blank=True)
    language = models.CharField(_('language'), max_length=15, default='en')  # passage language
    lang_ref = models.CharField(_('reference language'), max_length=15, default='en')  # reference language
    review_frequency = models.FloatField(_('review frequency'), default=2.0)
    review_limit = models.IntegerField(_('review limit'), default=128)
    daily_goal = models.IntegerField(_('daily goal'), default=70)
    review_batch = models.IntegerField(default=5)
    inverse_limit = models.IntegerField(default=7)
    last_score_purge = models.DateField(null=True, blank=True)
    reference_included = models.BooleanField(default=False)
    topic_preferred = models.BooleanField(default=False)
    default_source = models.CharField(max_length=250, null=True, blank=True)

    class Canon(models.IntegerChoices):
        PROTESTANT = 0
        LUTHERAN = 1
        ROMAN = 2
        EASTERN = 3
        ORIENTAL = 4
        JEWISH = 5
    canon = models.IntegerField(choices=Canon.choices, default=Canon.PROTESTANT)

    class FontType(models.IntegerChoices):
        SANS = 0
        SERIF = 1
        MONO = 2

    font_type = models.IntegerField(choices=FontType.choices, default=FontType.SANS)

    class VerseOrder(models.IntegerChoices):
        ALPHABET = 0
        CANON = 1
        TOPIC = 2
        DATE = 3
        LEVEL = 4
        RANDOM = 5

    order_new = models.IntegerField(choices=VerseOrder.choices, default=VerseOrder.DATE)
    order_due = models.IntegerField(choices=VerseOrder.choices, default=VerseOrder.LEVEL)
    order_known = models.IntegerField(choices=VerseOrder.choices, default=VerseOrder.RANDOM)
    order_all = models.IntegerField(choices=VerseOrder.choices, default=VerseOrder.CANON)

    # imported_decks defined in Deck
    # verses defined in Verse
    # tags defined in Tag

    @property
    def own_decks(self):
        return Deck.objects.filter(tag__account=self)

    def purge_scores(self):
        """Delete all Scores except last 9 days + highscore days
        """
        date_limit = date.today() - timedelta(days=9)
        highscore_dates = self.__highscore_dates()
        query = Score.objects.filter(account=self).filter(date__lte=date_limit)
        for dat in highscore_dates:
            query = query.exclude(date=dat)
        query.delete()
        self.last_score_purge = date.today()
        self.save()

    def __highscore_dates(self):
        return [x[0] for x in self.__highscores()]

    def __highscores(self):
        return sorted(self.__scores().items(), key=lambda x: -x[1])[:7]

    def __scores(self):
        score_dict = {}
        ops = Score.objects.filter(account=self)
        for op in ops:
            if op.date not in score_dict.keys():
                score_dict[op.date] = op.change
            else:
                score_dict[op.date] = score_dict[op.date] + op.change
        return score_dict

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = _('account')
        verbose_name_plural = _('accounts')
        db_table = 'main_account'
        indexes = [
            models.Index(fields=['name'], name='account_name_idx'),
        ]
        ordering = ['name']


class Verse(Replicable, Accountable):
    tags = models.ManyToManyField(Tag, related_name='verses', blank=True)
    reference = models.CharField(max_length=500)
    source = models.CharField(max_length=250, null=True, blank=True)
    topic = models.CharField(max_length=1000, null=True, blank=True)
    passage = models.CharField(max_length=100000)
    review = models.DateField(null=True, blank=True)
    commit = models.DateField(null=True, blank=True)
    level = models.IntegerField(default=-1)
    image = models.CharField(max_length=1500, null=True, blank=True)
    note = models.CharField(max_length=50000, null=True, blank=True)
    @property
    def safe_image(self):
        return self.image if self.image and self.image.startswith('https://images.unsplash.com/') else None

    def __str__(self):
        return '%s / %s' % (self.reference, self.account)

    class Meta:
        constraints = [
            models.CheckConstraint(check=~models.Q(reference=""), name="non_empty_reference"),
            models.CheckConstraint(check=~models.Q(passage=""), name="non_empty_passage")
        ]
        indexes = [
            models.Index(fields=['account', 'modified_on_server'], name='verse_modified_idx'),
            models.Index(fields=['account', 'deleted', 'reference', 'source'], name='verse_deleted_idx'),
        ]
        ordering = ['reference', 'account__name']


class Score(Replicable, Accountable):
    date = models.DateField()
    vkey = models.CharField(max_length=500)  # reference + source
    change = models.IntegerField()
    deleted_id = models.BigIntegerField(default=0)
    deleted = models.GeneratedField(
        expression=Case(When(deleted_id__gt=0, then=True), default=False),
        output_field=models.BooleanField(),
        db_persist=True,
    )

    def __str__(self):
        return '%s / %s (%s, %s)' % (self.date, self.vkey, self.change, self.account)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['account', 'date', 'vkey', 'deleted_id'], name='unique_score'),
        ]
        indexes = [
            models.Index(fields=['account', 'modified_on_server'], name='score_modified_on_server_idx'),
            models.Index(fields=['account', 'modified'], name='score_modified_idx'),
            models.Index(fields=['account', 'date'], name='score_date_idx'),
        ]
        ordering = ['-date', 'vkey']
