import logging

from django.db.models import Count, QuerySet, Q
from django.shortcuts import get_object_or_404
from langcodes import Language, LanguageTagError
from rest_framework import viewsets, permissions, generics
from rest_framework.exceptions import PermissionDenied
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import AllowAny
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.throttling import UserRateThrottle

from .models import Account, Verse, Tag, Score, Deck
from .renderers import UrlXmlRenderer, FeedXmlRenderer
from .serializers import AccountSerializer, TagSerializer, VerseSerializer, ScoreSerializer, CollectionSerializer, \
    CollectionVerseSerializer, DeckSerializer, CollectionDetailSerializer, CollectionUrlSerializer, \
    CollectionFeedSerializer
from .util import now

logger = logging.getLogger('app')


def filter_deleted(request: Request, query: QuerySet):
    deleted = request.query_params.get('deleted')
    if deleted is not None:
        if deleted in ['true', 'True', 't', 'T', '1']:
            deleted = True
        elif deleted in ['false', 'False', 'f', 'F', '0']:
            deleted = False
        else:
            deleted = None
    if deleted is not None:
        return query.filter(deleted=deleted)
    return query


def filter_account(request: Request, query: QuerySet):
    account = request.query_params.get('account')
    if account:
        return query.filter(account__id=account)
    return query


class IsOwner(permissions.BasePermission):

    def has_permission(self, request, view):
        if 'tag' not in request.data:
            return True
        try:
            tag = Tag.objects.select_related('account__user').get(id=request.data['tag'])
            return tag.account.user == request.user
        except Tag.DoesNotExist:
            return False

    def has_object_permission(self, request, view, obj):
        try:
            return obj.user == request.user
        except AttributeError:
            try:
                return obj.account.user == request.user
            except AttributeError:
                return obj.tag.account.user == request.user

class ReplicableViewSet(viewsets.ModelViewSet):
    """Query params: since (<=modified), client (!=modified_by), deleted"""
    model_class = None

    def get_queryset(self) -> QuerySet:
        query = self.model_class.objects
        since = self.request.query_params.get('since')
        if since:
            query = query.filter(modified_on_server__gte=int(since))
        client: str = self.request.query_params.get('client')
        if client:
            query = query.exclude(modified_by=client)
        return filter_deleted(self.request, query)

    def get_serializer(self, *args, **kwargs):
        """ sets serializer to 'many' if a list is passed """
        if isinstance(kwargs.get('data', {}), list):
            kwargs['many'] = True  # -> ListSerializer
        return super().get_serializer(*args, **kwargs)

    def put(self, request, *args, **kwargs):
        """ updates a list or a single instance completely """
        if isinstance(request.data, list):
            return self.update_all(request, *args, **kwargs)
        else:
            return self.update(request, *args, **kwargs)  # requires 'pk' in url

    def patch(self, request, *args, **kwargs):
        """ updates a list or a single instance partially """
        kwargs['partial'] = True
        return self.put(request, *args, **kwargs)

    def update_all(self, request, *args, **kwargs):
        """ updates an existing list instance """
        partial = kwargs.pop('partial', False)
        allowed_account_ids = Account.objects.filter(user=request.user).values_list('id', flat=True)
        if self.model_class == Account:
            instance = self.model_class.objects.filter(user=request.user)
            data = list(filter(lambda account: account['id'] in allowed_account_ids, request.data))
        else:
            instance = self.model_class.objects.filter(account__user=request.user)
            data = list(filter(lambda item: item['account'] in allowed_account_ids, request.data))
        serializer = self.get_serializer(instance, data=data, partial=partial)
        is_valid = serializer.is_valid(raise_exception=False)
        if is_valid:
            self.perform_update(serializer)
            return Response(serializer.data)
        else:
            logger.error(serializer.errors, exc_info=True)
            return Response(data)

    def __remove_foreign_items(self, data):
        allowed_account_ids = Account.objects.values_list('id', flat=True)
        if self.model_class == Account:
            return list(filter(lambda account: account['id'] in allowed_account_ids, data))
        else:
            return list(filter(lambda item: item['account_id'] in allowed_account_ids, data))


class AccountViewSet(ReplicableViewSet):
    model_class = Account
    serializer_class = AccountSerializer
    permission_classes = [IsOwner]
    ordering_fields = ['name']

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return super().get_queryset().filter(user=user).select_related('user').order_by('name')
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


class AccountableViewSet(ReplicableViewSet):
    """Query params: account (id)"""
    permission_classes = [IsOwner]

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            query = super().get_queryset().filter(account__user=user).select_related('account')
            return filter_account(self.request, query)
        raise PermissionDenied()


class TagViewSet(AccountableViewSet):
    model_class = Tag
    serializer_class = TagSerializer


class VerseViewSet(AccountableViewSet):
    model_class = Verse
    serializer_class = VerseSerializer


class ScoreViewSet(AccountableViewSet):
    model_class = Score
    serializer_class = ScoreSerializer


class DeckViewSet(viewsets.ModelViewSet):
    permission_classes = [IsOwner]
    model_class = Deck
    serializer_class = DeckSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return Deck.objects.filter(tag__account__user=user).select_related('tag__account')
        raise PermissionDenied()

    def create(self, request, *args, **kwargs):
        self.check_permissions(request)
        return super(DeckViewSet, self).create(request)

    def perform_create(self, serializer):
        self.__update_tag(serializer.validated_data['tag'])
        super(DeckViewSet, self).perform_create(serializer)

    def perform_destroy(self, instance):
        self.__update_tag(instance.tag)
        super(DeckViewSet, self).perform_destroy(instance)

    def __update_tag(self, tag: Tag):
        timestamp = now()
        tag.modified = timestamp
        tag.modified_on_server = timestamp
        tag.save()


class CollectionList(generics.ListAPIView):
    permission_classes = [AllowAny]
    throttle_classes = [UserRateThrottle]
    serializer_class = CollectionSerializer
    pagination_class = LimitOffsetPagination
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['name', 'description', 'tag__account__name']
    ordering_fields = ['name', 'created', 'downloads', 'size', 'featured']
    ordering = ['-featured', '-downloads']

    def get_queryset(self):
        """
        Filtered and paged list of published decks
        """
        queryset = Deck.objects.annotate(size=Count('tag__verses', filter=Q(tag__verses__deleted=False), distinct=True),
                                         downloads=Count('importers', distinct=True)).select_related('tag__account')
        queryset = queryset.filter(tag__deleted=False, size__gt=0)
        language: str = self.request.query_params.get('language', None)
        imported_by: str = self.request.query_params.get('imported_by', None)
        if language is not None:
            queryset = queryset.filter(tag__account__language__startswith=language)
        if imported_by is not None:
            queryset = queryset.filter(importers__id=imported_by)
        return queryset


class CollectionVerseList(generics.ListAPIView):
    permission_classes = [AllowAny]
    throttle_classes = [UserRateThrottle]
    serializer_class = CollectionVerseSerializer

    def get_queryset(self):
        """
        Filtered and paged list of published decks
        """
        tag_id = self.kwargs['pk']
        tag = get_object_or_404(Tag, id=tag_id)
        return tag.verses.filter(deleted=False)


class CollectionView(RetrieveModelMixin, generics.GenericAPIView):
    permission_classes = [AllowAny]
    throttle_classes = [UserRateThrottle]
    serializer_class = CollectionDetailSerializer
    queryset = Deck.objects.annotate(size=Count('tag__verses', filter=Q(tag__verses__deleted=False), distinct=True),
                                     downloads=Count('importers', distinct=True))

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class CollectionSitemap(generics.ListAPIView):
    permission_classes = [AllowAny]
    throttle_classes = [UserRateThrottle]
    serializer_class = CollectionUrlSerializer
    renderer_classes = [UrlXmlRenderer]
    queryset = Deck.objects.annotate(
        size=Count('tag__verses', filter=Q(tag__verses__deleted=False), distinct=True),
        downloads=Count('importers', distinct=True)
    ).filter(tag__deleted=False, size__gt=0).order_by('-downloads')


class CollectionFeed(generics.ListAPIView):
    permission_classes = [AllowAny]
    throttle_classes = [UserRateThrottle]
    serializer_class = CollectionFeedSerializer
    renderer_classes = [FeedXmlRenderer]

    def get_queryset(self):
        """
        List of published decks, filtered by locale
        """
        queryset = Deck.objects.annotate(
            size=Count('tag__verses', filter=Q(tag__verses__deleted=False), distinct=True),
        ).filter(tag__deleted=False, size__gt=0).select_related('tag__account')
        try:
            locale = Language.get(self.kwargs['locale'])
            self.request.accepted_renderer.sub_root_data['language'] = locale.language
            queryset = queryset.filter(tag__account__language__startswith=locale.language)
        except (KeyError, LanguageTagError, AttributeError):
            pass
        return queryset.order_by('-created')[:999]
