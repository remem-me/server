from django.core.management import BaseCommand

from main.models import Verse, Score, Tag, Account
from main.util import now


class Command(BaseCommand):
    help = 'Deletes all entities marked longer than a certain number of days ago. ' \
           'Usage: python $HOME/rm_replication/manage.py cleardeleted 100 --settings=rm_replication.settings.prod'

    def add_arguments(self, parser):
        parser.add_argument('days', type=int)  # for list add nargs='+' or nargs='3' (length must be 3)

    def handle(self, *args, **options):
        days = int(options['days'])
        timestamp = now() - days * 24 * 60 * 60 * 1000
        for manager in [Score.objects, Verse.objects, Tag.objects, Account.objects]:
            (deleted, num) = manager.filter(deleted=True, modified__lt=timestamp).delete()
            # num: e.g. {'main.Verse_tags': 1, 'main.Verse': 4}
            self.stdout.write(self.style.SUCCESS('Cleared %s after more than %d days: %s' % (manager.model, days, num)))
