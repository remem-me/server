import datetime

from rest_framework.test import APIRequestFactory, APITestCase
from rest_framework.test import force_authenticate

from access.models import User
from main.models import Account, Verse, Tag, Deck, Score
from main.serializers import VerseSerializer, ReplicableListSerializer, ScoreSerializer, ScoreListSerializer
from main.views import VerseViewSet, AccountViewSet, DeckViewSet, ScoreViewSet


class SanityTest(APITestCase):

    def test_sanity(self):
        """Make sure testing works"""
        self.assertTrue(1 == 1)

class BaseTestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create(email="test@remem.me", password="test", is_active=True)
        self.otherUser = User.objects.create(email="other.test@remem.me", password="test", is_active=True)
        Account.objects.create(id=1, user=self.user, modified=0, name="my account")
        Account.objects.create(id=2, user=self.otherUser, modified=0, name="not my account")
        Verse.objects.create(id=1, account_id=1, reference='Existing1', passage='1 exists.', modified=0)
        Verse.objects.create(id=2, account_id=2, reference='Existing2', passage='2 exists.', modified=0)
        Verse.objects.create(id=3, account_id=2, reference='Existing3', passage='3 exists.', modified=0)
        Tag.objects.create(id=1, modified=0, account_id=1, text='Tag1')
        Tag.objects.create(id=2, modified=0, account_id=2, text='Tag2')
        Tag.objects.create(id=3, modified=0, account_id=1, text='Tag3')
        Tag.objects.create(id=4, modified=0, account_id=2, text='Tag4')
        Deck.objects.create(tag_id=1, name='Collection1', description='description1')
        Deck.objects.create(tag_id=4, name='Collection4', description='description4')
        Score.objects.create(id=1, account_id=1, date=datetime.datetime(2024, 1, 1), vkey='1', change=1, modified=0)
        Score.objects.create(id=2, account_id=2, date=datetime.datetime(2024, 1, 2), vkey='2', change=2, modified=0)
        Score.objects.create(id=3, account_id=1, date=datetime.datetime(2024, 1, 3), vkey='1', change=3, deleted_id=3,
                             modified=0)


class AccountViewTestCase(BaseTestCase):

    def test_user_can_edit_own_account_bot_not_other(self):
        view = AccountViewSet.as_view({'patch': 'update_all'})
        request = APIRequestFactory().patch(
            '/v1/accounts/1/',
            data=[{'id': 1, 'modified': 1, 'modified_by': 'device1', 'name': 'updated1', 'imported_decks': []},
                  {'id': 2, 'modified': 1, 'modified_by': 'device2', 'name': 'updated2', 'imported_decks': []}],
            format='json')
        force_authenticate(request, user=self.user)
        response = view(request, pk=1)
        response.render()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Account.objects.get(id=1).name, 'updated1')
        self.assertEqual(Account.objects.get(id=2).name, 'not my account')


class VerseSerializerTestCase(BaseTestCase):

    def test_verse_list_is_created(self):
        data = [{'id': 11, 'account': 1, 'reference': 'New', 'passage': 'New.', 'modified': 0}]
        instance = Verse.objects.filter(account__user=self.user)
        serializer = ReplicableListSerializer(instance, data=data, partial=True, child=VerseSerializer())
        self.assertTrue(serializer.is_valid(raise_exception=False))
        self.assertEqual(serializer.errors, [])
        verses = serializer.save()
        self.assertEqual(verses[0].passage, data[0]['passage'])

    def test_verse_list_is_updated_inspite_of_integrity_error(self):
        data = [{'id': 2, 'account': 1, 'reference': 'Updated', 'passage': 'Updated.', 'modified': 1}]
        instance = Verse.objects.filter(account__user=self.user)  # simulating verse id of different user
        serializer = ReplicableListSerializer(instance, data=data, partial=True, child=VerseSerializer())
        self.assertTrue(serializer.is_valid(raise_exception=False))
        self.assertEqual(serializer.errors, [])
        verses = serializer.save()
        self.assertEqual(len(verses), 0)
        self.assertEqual(len(Verse.objects.filter(account__user=self.user)), 1)


class VerseViewTestCase(BaseTestCase):

    def test_user_can_edit_own_verses_but_not_other(self):
        view = VerseViewSet.as_view({'patch': 'update_all'})
        request = APIRequestFactory().patch(
            '/v1/verses/',
            data=[
                {'id': 1, 'account': 1, 'reference': 'updated', 'passage': 'updated', 'modified': 1},
                {'id': 2, 'account': 2, 'reference': 'updated', 'passage': 'updated', 'modified': 1},
                {'id': 3, 'account': 1, 'reference': 'updated', 'passage': 'updated', 'modified': 1},
            ],
            format='json')
        force_authenticate(request, user=self.user)
        response = view(request)
        response.render()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Verse.objects.get(id=1).passage, 'updated')
        self.assertEqual(Verse.objects.get(id=2).passage, '2 exists.')
        self.assertEqual(Verse.objects.get(id=3).passage, '3 exists.')

    def test_user_can_add_note_to_verse(self):
        view = VerseViewSet.as_view({'patch': 'update_all'})
        request = APIRequestFactory().patch(
            '/v1/verses/',
            data=[
                {'id': 1, 'account': 1, 'reference': 'updated', 'passage': 'updated', 'note': 'note 1', 'modified': 1},
            ],
            format='json')
        force_authenticate(request, user=self.user)
        response = view(request)
        response.render()
        self.assertEqual(response.status_code, 200)
        self.assertEqual( Verse.objects.get(id=1).note, 'note 1')

    def test_verse_with_valid_and_invalid_tags(self):
        view = VerseViewSet.as_view({'patch': 'update_all'})
        request = APIRequestFactory().patch(
            '/v1/verses/',
            data=[
                {'id': 1, 'account': 1, 'reference': 'updated', 'passage': 'updated', 'modified': 1,
                 'tags': [1, 3, 99]}
            ],
            format='json')
        force_authenticate(request, user=self.user)
        response = view(request)
        response.render()
        self.assertEqual(response.status_code, 200)
        verse = Verse.objects.get(id=1)
        self.assertEqual(verse.tags.count(), 2)
        self.assertTrue(Tag.objects.get(id=1) in verse.tags.all())
        self.assertTrue(Tag.objects.get(id=3) in verse.tags.all())


class DeckViewTestCase(BaseTestCase):

    def test_user_can_create_own_deck(self):
        view = DeckViewSet.as_view({'post': 'create'})  # debug APIView.dispatch
        request = APIRequestFactory().post(
            '/v1/decks/',
            data={'tag': 3, 'name': 'Collection3', 'description': 'new description'},
            format='json')
        force_authenticate(request, user=self.user)
        response = view(request, pk=3)
        response.render()
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Deck.objects.get(tag=3).description, 'new description')

    def test_user_can_edit_own_deck(self):
        view = DeckViewSet.as_view({'post': 'update'})  # debug APIView.dispatch
        request = APIRequestFactory().post(
            '/v1/decks/',
            data={'tag': 1, 'name': 'Collection1', 'description': 'updated description'},
            format='json')
        force_authenticate(request, user=self.user)
        response = view(request, pk=1)
        response.render()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Deck.objects.get(tag=1).description, 'updated description')

    def test_user_cannot_create_other_deck(self):
        view = DeckViewSet.as_view({'post': 'create'})  # debug APIView.dispatch
        request = APIRequestFactory().post(
            '/v1/decks/',
            data={'tag': 2, 'name': 'Collection2', 'description': 'updated description'},
            format='json')
        force_authenticate(request, user=self.user)
        response = view(request, pk=2)
        response.render()
        self.assertEqual(response.status_code, 403)

    def test_user_cannot_edit_other_deck(self):
        view = DeckViewSet.as_view({'post': 'update'})  # debug APIView.dispatch
        request = APIRequestFactory().post(
            '/v1/decks/',
            data={'tag': 4, 'name': 'Collection4', 'description': 'updated description'},
            format='json')
        force_authenticate(request, user=self.user)
        response = view(request, pk=4)
        response.render()
        self.assertEqual(response.status_code, 403)


class ScoreSerializerTestCase(BaseTestCase):

    def test_score_list_is_created(self):
        data = [{'id': 4, 'account': 1, 'date': '2024-01-04', 'vkey': '1', 'change': 4, 'modified': 1}]
        instance = Score.objects.filter(account__user=self.user)
        serializer = ReplicableListSerializer(instance, data=data, partial=True, child=ScoreSerializer())
        self.assertTrue(serializer.is_valid(raise_exception=False))
        self.assertEqual(serializer.errors, [])
        scores = serializer.save()
        self.assertEqual(scores[0].change, data[0]['change'])

    def test_score_is_deleted(self):
        data = [{'id': 1, 'account': 1, 'date': '2024-01-03', 'vkey': '1', 'change': 1, 'deleted': True, 'modified': 1}]
        instance = Score.objects.filter(account__user=self.user)
        serializer = ReplicableListSerializer(instance, data=data, partial=True, child=ScoreSerializer())
        self.assertTrue(serializer.is_valid(raise_exception=False))
        self.assertEqual(serializer.errors, [])
        scores = serializer.save()
        self.assertEqual(scores[0].deleted_id, data[0]['id'])

    def test_previous_score_on_same_day_is_overwritten(self):
        data = [
            {'id': 1, 'account': 1, 'date': '2024-01-03', 'vkey': '1', 'change': 1, 'modified': 1},
            {'id': 4, 'account': 1, 'date': '2024-01-03', 'vkey': '1', 'change': 4, 'modified': 2}
        ]
        instance = Score.objects.filter(account__user=self.user)
        serializer = ScoreListSerializer(instance, data=data, partial=True, child=ScoreSerializer())
        self.assertTrue(serializer.is_valid(raise_exception=False))
        self.assertEqual(serializer.errors, [])
        serializer.save()
        scores = Score.objects.filter(account__user=self.user, vkey__exact='1', date__exact='2024-01-03', deleted=0)
        self.assertEqual(len(scores), 1)
        self.assertEqual(scores[0].change, 4)

    def test_previous_score_on_same_day_is_not_overwritten(self):
        data = [
            {'id': 1, 'account': 1, 'date': '2024-01-03', 'vkey': '1', 'change': 1, 'modified': 2},
            {'id': 4, 'account': 1, 'date': '2024-01-03', 'vkey': '1', 'change': 4, 'modified': 1}
        ]
        instance = Score.objects.filter(account__user=self.user)
        serializer = ScoreListSerializer(instance, data=data, partial=True, child=ScoreSerializer())
        self.assertTrue(serializer.is_valid(raise_exception=False))
        self.assertEqual(serializer.errors, [])
        serializer.save()
        scores = Score.objects.filter(account__user=self.user, vkey__exact='1', date__exact='2024-01-03', deleted=0)
        self.assertEqual(len(scores), 1)
        self.assertEqual(scores[0].change, 1)


class ScoreViewTestCase(BaseTestCase):

    def test_user_can_create_score(self):
        view = ScoreViewSet.as_view({'post': 'create'})
        request = APIRequestFactory().post(
            '/v1/scores/',
            data=[{'id': 4, 'account': 1, 'date': '2024-01-04', 'vkey': '1', 'change': 4, 'modified': 1}],
            format='json')
        force_authenticate(request, user=self.user)
        response = view(request, pk=3)
        response.render()
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Score.objects.get(id=4).change, 4)

    def test_user_can_delete_score(self):
        view = ScoreViewSet.as_view({'patch': 'update_all'})
        request = APIRequestFactory().patch(
            '/v1/scores/',
            data=[
                {'id': 1, 'account': 1, 'date': '2024-01-03', 'vkey': '1', 'change': 1, 'deleted': True, 'modified': 1},
            ],
            format='json')
        force_authenticate(request, user=self.user)
        response = view(request)
        response.render()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Score.objects.get(id=1).deleted_id, 1)
