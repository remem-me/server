#!/bin/bash

###
# Git Hook
# ~/rm_replication.git/hooks/post-receive
# May need "dos2unix rm_replication.git/hooks/post-receive".
# (https://stackoverflow.com/a/26767706)
###

### CONFIG VARS
TARGET="$HOME/rm_replication"
GIT_DIR="$HOME/rm_replication.git"
BRANCH="master"
RESTART=true
API_KEY=
ACCOUNT=
SITE_ID=
SETTINGS="rm_replication.settings.prod"

while read oldrev newrev ref
do
    if [[ $ref = refs/heads/$BRANCH ]];
    then
        echo "Ref '$ref' received."

        if [ ! -d "$TARGET" ];
        then
            echo "'${TARGET}' dir is missing, creating it"
            mkdir -p $TARGET
        fi

        echo "Deploying '${BRANCH}' branch to production"
        git --work-tree=$TARGET --git-dir=$GIT_DIR checkout --force $BRANCH

        echo "Installing dependencies"
        source $TARGET/venv/bin/activate
        pip install --upgrade pip
        pip install -r $TARGET/requirements.txt

	      # echo "Migrating database"
        # python $TARGET/manage.py migrate --settings=$SETTINGS

        echo "Collecting static files"
        python $TARGET/manage.py collectstatic --no-input --clear --link --settings=$SETTINGS


        if [ "$RESTART" = true ];
        then
            echo "Restarting your web server"
            status=$(curl --basic --user "${API_KEY} account=${ACCOUNT}:" --data '' --request POST --silent --output /dev/null --write-out '%{http_code}' "https://api.alwaysdata.com/v1/site/${SITE_ID}/restart/")
            if [ "$status" = 204 ];
            then
                echo "Your site is restarted"
            else
                echo "An error occurred when restarting your site, try to restart it manually"
                exit 1
            fi
        fi

        exit 0
    else
        echo "You pushed '$ref' branch,"
        echo "but you set '${BRANCH}' branch as deploy branch."
        echo "Exiting without error."

        exit 0
    fi
done
