## Contributing

First off, thank you for considering contributing to Remember Me. This is a great 
way to serve your sisters and brothers in Christ.

### Where do I go from here?

If you've noticed a bug or have a question that doesn't belong on the
[user forum][], [search the issue tracker][] to see if
someone else in the community has already created a ticket. If not, go ahead and
[make one][new issue]!

### Fork & create a branch

If this is something you think you can fix, then [fork Remember Me][] and
create a branch with a descriptive name.

A good branch name would be (where issue #325 is the ticket you're working on):

```sh
git checkout -b 325-add-japanese-translations
```

### Get the test suite running

Start the development server to make it listen on all interfaces
```sh
python manage.py runserver 0.0.0.0:8000
```


### Did you find a bug?

* **Ensure the bug was not already reported** by [searching all issues][].

* If you're unable to find an open issue addressing the problem,
  [open a new one][new issue]. Be sure to include a **title and clear
  description**, as much relevant information as possible, and a **code sample**
  or an **executable test case** demonstrating the expected behavior that is not
  occurring.

### 5. Implement your fix or feature

At this point, you're ready to make your changes! Feel free to ask for help;
everyone is a beginner at first :smile_cat:

### Get the style right

Your patch should follow the same conventions & pass the same code quality
checks as the rest of the project. 

### Make a Pull Request

At this point, you should switch back to your master branch and make sure it's
up to date with Remember Me's master branch:

```sh
git remote add upstream git@gitlab.com:remem-me/server.git
git checkout master
git pull upstream master
```

Then update your feature branch from your local copy of master, and push it!

```sh
git checkout 325-add-japanese-translations
git rebase master
git push --set-upstream origin 325-add-japanese-translations
```

Finally, go to GitLab and [make a Pull Request][] :D

CI will run our test suite. We care
about quality, so your PR won't be merged until all tests pass. 

### Keeping your Pull Request updated

If a maintainer asks you to "rebase" your PR, they're saying that a lot of code
has changed, and that you need to update your branch so it's easier to merge.

To learn more about rebasing in Git, there are a lot of [good resources][git rebasing] 
but here's the suggested workflow:

```sh
git checkout 325-add-japanese-translations
git pull --rebase upstream master
git push --force-with-lease 325-add-japanese-translations
```



[user forum]: https://groups.google.com/forum/#!forum/rememberapp
[search the issue tracker]: https://gitlab.com/remem-me/server/issues
[new issue]: https://gitlab.com/remem-me/server/issues/new
[fork Remember Me]: https://docs.gitlab.com/ee/gitlab-basics/fork-project.html
[searching all issues]: https://gitlab.com/remem-me/server/issues
[make a pull request]: https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html
[git rebasing]: http://git-scm.com/book/en/Git-Branching-Rebasing
[interactive rebase]: https://help.github.com/articles/interactive-rebase
[codecov]: https://codecov.io/gl/remem-me/app