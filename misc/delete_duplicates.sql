SELECT reference, source, account_id, name
from main_verse v
         JOIN main_account a ON account_id = a.id
WHERE name = ?
group by account_id, reference, source, name
having count(*) > 1;

SELECT reference, source, passage, account_id
FROM main_verse
WHERE id IN (SELECT id
             FROM (SELECT id,
                          ROW_NUMBER() OVER (PARTITION BY reference, source, passage, account_id) AS rownum
                   FROM main_verse
                   WHERE account_id = ?) AS sub
             WHERE rownum > 1);


DELETE
FROM main_verse
WHERE id IN (SELECT id
             FROM (SELECT id,
                          ROW_NUMBER() OVER (PARTITION BY reference, source, passage, account_id) AS rownum
                   FROM main_verse
                   WHERE account_id = ?) AS sub
             WHERE rownum > 1);


DELETE
FROM main_verse_tags
WHERE verse_id IN (SELECT id
                   FROM (SELECT id,
                                ROW_NUMBER() OVER (PARTITION BY reference, source, passage, account_id) AS rownum
                         FROM main_verse
                         WHERE account_id = ?) AS sub
                   WHERE rownum > 1);