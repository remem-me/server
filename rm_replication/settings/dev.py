from rm_replication.settings.base import *

DEBUG = True

SECRET_KEY = 'my_secret_key'

ALLOWED_HOSTS = ['air.local', '10.0.2.2']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'remem-me_db',
        'USER': 'remem-me_rm',
        'PASSWORD': 'my_secret',
        'HOST': 'localhost',
        'PORT': '5432',
        'ATOMIC_REQUESTS': True,
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

REST_FRAMEWORK['DEFAULT_THROTTLE_RATES'] = {
    'anon': '5000/day',
    'user': '10000/day'
}
